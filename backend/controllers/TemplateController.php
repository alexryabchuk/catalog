<?php

namespace backend\controllers;

use common\models\entity\Template;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller
 */
class TemplateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['set-default', 'edit-php', 'edit-smarty', 'edit-blade', 'index','view-php','view-smarty'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Template::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Установка шаблона по умолчанию
     *
     * @return string
     */
    public function actionSetDefault($id, $default_template)
    {
        $template = Template::findOne((int)$id);
        if ($template) {
            $template->default_template = (int)$default_template;
            $template->save();
        }
        return $this->redirect(['/template/index']);
    }

    /**
     * Редактирование шаблона РНР
     *
     * @param $id
     * @return string
     */
    public function actionEditPhp($id)
    {
        $model = Template::findOne((int)$id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/template/index']);
        }
        return $this->render('edit-php', [
            'model' => $model
        ]);
    }

    /**
     * Редактирование шаблона РНР
     *
     * @param $id
     * @return string
     */
    public function actionViewPhp($id)
    {
        $model = Template::findOne((int)$id);
        /* @var $model Template */

        $fp = fopen( Yii::getAlias("@template/file.php"), "w");
        fwrite($fp, $model->template_php);
        fclose($fp);

        return $this->render("@template/file", [
            'model' => $model
        ]);
    }

    /**
     * Редактирование шаблона Smarty
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionEditSmarty($id)
    {
        $model = Template::findOne((int)$id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/template/index']);
        }
        return $this->render('edit-smarty', [
            'model' => $model
        ]);
    }

    /**
     * Редактирование шаблона РНР
     *
     * @param $id
     * @return string
     */
    public function actionViewSmarty($id)
    {
        $model = Template::findOne((int)$id);
        /* @var $model Template */

        $fp = fopen( Yii::getAlias("@template/file.tpl"), "w");
        fwrite($fp, $model->template_smarty);
        fclose($fp);

        return $this->render("@template/file.tpl", [
            'model' => $model
        ]);
    }

    /**
     * Редактирование шаблона Blade
     *
     * @param $id
     * @return string
     */
    public function actionEditBlade($id)
    {
        $model = Template::findOne((int)$id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/template/index']);
        }
        return $this->render('edit-blade', [
            'model' => $model
        ]);
    }


}
