<?php

namespace backend\controllers;

use backend\models\search\SponsorPromoSearch;
use common\helpers\ColumnVisible;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * PaymentsSales controller
 */
class PromoController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница отображения списка продаж
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter = ColumnVisible::getColumnVisible('sponsor_promo', [
            'sponsor_id' => 'on',
            'name' => 'on',
            'image' => 'on',
            'content' => 'on',
            'start_date' => 'on',
            'end_date' => 'on',
            'url' => 'on',
            'sites' => 'on'
        ]);
        $searchModel = new SponsorPromoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
        ]);
    }

}
