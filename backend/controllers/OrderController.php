<?php

namespace backend\controllers;

use backend\models\search\SponsorOrderReviewSearch;
use common\helpers\ColumnVisible;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * PaymentsSales controller
 */
class OrderController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница отображения списка продаж
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter = ColumnVisible::getColumnVisible('sponsor_order_review', [
            'sponsor_id' => 'on',
            'status' => 'on',
            'order_date' => 'on',
            'e_mail' => 'on',
            'order_sponsor_id' => 'on',
            'order_url' => 'on',
            'recip_URL' => 'on',
            'editor' => 'on',
            'writer' => 'on',
            'pay_amount' => 'on',
            'pay_status' => 'on',
        ]);
        $searchModel = new SponsorOrderReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
        ]);
    }

}
