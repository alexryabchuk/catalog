<?php

namespace backend\controllers;

use backend\models\search\SponsorsSitesSearch;
use common\helpers\ColumnVisible;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * PaymentsSales controller
 */
class SitesController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница отображения списка продаж
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter = ColumnVisible::getColumnVisible('sponsor_post', [
            'sponsor_id' => 'on',
            'name' => 'on',
            'pps_status' => 'on',
            'main_category' => 'on',
            'add_category' => 'on',
        ]);
        $searchModel = new SponsorsSitesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
        ]);
    }

}
