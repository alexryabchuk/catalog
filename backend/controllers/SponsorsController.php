<?php

namespace backend\controllers;

use backend\models\search\SponsorOrderReviewSearch;
use backend\models\search\SponsorPromoSearch;
use backend\models\search\SponsorsAccessSearch;
use backend\models\search\SponsorsBacklinksSearch;
use backend\models\search\SponsorsPaymentsHistorySearch;
use backend\models\search\SponsorsPaymentsMethodSearch;
use backend\models\search\SponsorsPaymentsProgramsSearch;
use backend\models\search\SponsorsPaymentsSalesSearch;
use backend\models\search\SponsorsPostSearch;
use backend\models\search\SponsorsReviewSearch;
use backend\models\search\SponsorsSearch;
use backend\models\search\SponsorsSitesSearch;
use backend\models\search\SponsorsSupportSearch;
use common\helpers\ColumnVisible;
use common\models\entity\SponsorAccess;
use common\models\entity\SponsorBacklinks;
use common\models\entity\SponsorOrderReview;
use common\models\entity\SponsorPaymentsHistory;
use common\models\entity\SponsorPaymentsMethod;
use common\models\entity\SponsorPaymentsProgram;
use common\models\entity\SponsorPaymentsSales;
use common\models\entity\SponsorPost;
use common\models\entity\SponsorPromo;
use common\models\entity\Sponsors;
use common\models\entity\SponsorSites;
use common\models\entity\SponsorsReview;
use common\models\entity\SponsorSupport;
use common\models\enums\PaymentsMethodStatus;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SponsorsController extends Controller
{
    private $columnVisible = [
        'name' => 'on',
        'slug' => 'on',
        'ref_url' => 'on',
        'domain' => 'on',
        'description' => 'on',
        'publication_status' => 'on',
        'recommendation_status' => 'on',
        'not_recommendation_reason' => 'on',
        'general_company_id' => 'on',
        'cms_id' => 'on',
        'image' => 'on',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'create',
                            'update',
                            'delete',
                            'payments-method',
                            'payments-method-create',
                            'payments-method-update',
                            'payments-method-delete',
                            'payments-history',
                            'payments-history-create',
                            'payments-history-update',
                            'payments-history-delete',
                            'payments-programs',
                            'payments-programs-create',
                            'payments-programs-update',
                            'payments-programs-delete',
                            'payments-sales',
                            'payments-sales-create',
                            'payments-sales-update',
                            'payments-sales-delete',
                            'support',
                            'support-create',
                            'support-update',
                            'support-delete',
                            'sites',
                            'sites-create',
                            'sites-update',
                            'sites-delete',
                            'advertising',
                            'post',
                            'post-create',
                            'post-update',
                            'post-delete',
                            'access',
                            'access-create',
                            'access-update',
                            'access-delete',
                            'backlinks',
                            'backlinks-create',
                            'backlinks-update',
                            'backlinks-delete',
                            'review',
                            'review-create',
                            'review-update',
                            'review-delete',
                            'order-review',
                            'order-review-create',
                            'order-review-update',
                            'order-review-delete',
                            'promo',
                            'promo-create',
                            'promo-update',
                            'promo-delete',
                            'save-review-text',
                            'get-sponsor-sites'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница отображения списка пользователей
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter = ColumnVisible::getColumnVisible('sponsors', $this->columnVisible);
        $searchModel = new SponsorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
        ]);
    }

    /**
     * Создание пользователя
     *
     * @return string
     */
    public function actionCreate()
    {
        $model = new Sponsors();
        $model->publication_status = 1;
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->imageFile->saveAs("image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}");
                $model->image = "/image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}";
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Запись успешно создана');
                if  (Yii::$app->request->get('exit')!=1) {
                    return $this->redirect(['update','id'=>$model->id]);
                } else {
                    return $this->redirect(['index']) ;
                }
            }
            var_dump($model->errors);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование спонсора
     *
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        $model = Sponsors::findOne(intval($id));
        $model->imageFile = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->imageFile->saveAs("image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}");
                $model->image = "/image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}";
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Запись успешно сохранена');
                if  (Yii::$app->request->get('exit')==1) {
                    return $this->redirect(['index']);
                }
            } else {
                var_dump($model->errors);
            }

        }
        return $this->render('update', ['model' => $model]);
    }
    // платежние методи ****************************************************************

    /**
     * Список платежних систем спонсора
     *
     * @param $id
     * @return string
     */
    public function actionPaymentsMethod($sponsor_id)
    {
        $filter = ColumnVisible::getColumnVisible('payments_method', [
            'payment_system_id' => 'on',
            'min_pay' => 'on',
            'fee' => 'on',
            'hold' => 'on',
            'status' => 'on',
        ]);
        $searchModel = new SponsorsPaymentsMethodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('payments-method', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать платежних систем спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsMethodCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorPaymentsMethod();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать метод платежа",
                    'content' => $this->renderAjax('_form-payments_method', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать метод платежа",
                        'content' => $this->renderAjax('_form-payments_method', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_method', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование платежной системи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsMethodUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorPaymentsMethod::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать метод платежа",
                    'content' => $this->renderAjax('_form-payments_method', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать метод платежа",
                        'content' => $this->renderAjax('_form-payments_method', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_method', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование платежной системи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsMethodDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorPaymentsMethod */
        $model = SponsorPaymentsMethod::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // история платежей ****************************************************************

    /**
     * Список истории платежей спонсора
     *
     * @param $id
     * @return string
     */
    public function actionPaymentsHistory($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('payments_history', [
            'payments_method_id' => 'on',
            'date_pay' => 'on',
            'sum_pay' => 'on',
            'status' => 'on',
        ]);
        $searchModel = new SponsorsPaymentsHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('payments-history', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать платежн спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsHistoryCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorPaymentsHistory();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать историю платежа",
                    'content' => $this->renderAjax('_form-payments_history', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать историю платежа",
                        'content' => $this->renderAjax('_form-payments_history', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_history', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование платежа спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsHistoryUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorPaymentsHistory::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать историю платежа",
                    'content' => $this->renderAjax('_form-payments_history', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать историю платежа",
                        'content' => $this->renderAjax('_form-payments_history', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_history', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление платежна спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsHistoryDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorPaymentsHistory */
        $model = SponsorPaymentsHistory::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Программи платежей ****************************************************************

    /**
     * Список программ платежей спонсора
     *
     * @param $id
     * @return string
     */
    public function actionPaymentsPrograms($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('payments_programs', [
            'type' => 'on',
            'sum_percent' => 'on',
            'comment' => 'on',
        ]);
        $searchModel = new SponsorsPaymentsProgramsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('payments-programs', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать схему работы спартнером
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsProgramsCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorPaymentsProgram();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать схему работы с партнером",
                    'content' => $this->renderAjax('_form-payments_programs', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать схему работы с партнером",
                        'content' => $this->renderAjax('_form-payments_programs', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_programs', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование программи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsProgramsUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorPaymentsProgram::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать схему работы с партнером",
                    'content' => $this->renderAjax('_form-payments_programs', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать схему работы с партнером",
                        'content' => $this->renderAjax('_form-payments_programs', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_programs', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление программи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsProgramsDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorPaymentsProgram */
        $model = SponsorPaymentsProgram::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Программи платежей ****************************************************************

    /**
     * Список продаж спонсора
     *
     * @param $id
     * @return string
     */
    public function actionPaymentsSales($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('payments_sales', [
            'period' => 'on',
            'unq' => 'on',
            'sales' => 'on',
            'sales_usd' => 'on',
            'rebills' => 'on',
            'rebills_usd' => 'on',
        ]);
        $searchModel = new SponsorsPaymentsSalesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('payments-sales', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать продажу спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsSalesCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorPaymentsSales();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать продажу спонсора",
                    'content' => $this->renderAjax('_form-payments_sales', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать продажу спонсора",
                        'content' => $this->renderAjax('_form-payments_sales', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_sales', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование продажи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsSalesUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorPaymentsSales::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать продажу спонсора",
                    'content' => $this->renderAjax('_form-payments_sales', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать продажу спонсора",
                        'content' => $this->renderAjax('_form-payments_sales', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-payments_sales', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление продажи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPaymentsSalesDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorPaymentsSales */
        $model = SponsorPaymentsSales::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Связи спонсора ****************************************************************

    /**
     * Список связей спонсора
     *
     * @param $id
     * @return string
     */
    public function actionSupport($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_support', [
            'support_id' => 'on',
            'contact' => 'on',
            'response_time' => 'on',
            'language' => 'on',
            'comment' => 'on',
        ]);
        $searchModel = new SponsorsSupportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('support', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать связь спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionSupportCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorSupport();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать связь спонсора",
                    'content' => $this->renderAjax('_form-support', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать связь спонсора",
                        'content' => $this->renderAjax('_form-support', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-support', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование связи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionSupportUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorSupport::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать связь спонсора",
                    'content' => $this->renderAjax('_form-support', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать связь спонсора",
                        'content' => $this->renderAjax('_form-support', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-support', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление связи спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionSupportDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorSupport */
        $model = SponsorSupport::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Сайт ****************************************************************

    /**
     * Список сайтов спонсора
     *
     * @param $id
     * @return string
     */
    public function actionSites($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_sites', [
            'name' => 'on',
            'pps_status' => 'on',
            'main_category' => 'on',
            'add_category' => 'on',
        ]);
        $searchModel = new SponsorsSitesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('sites', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать сайт спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionSitesCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorSites();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать сайт",
                    'content' => $this->renderAjax('_form-sites', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать сайт",
                        'content' => $this->renderAjax('_form-sites', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-sites', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование сайта спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionSitesUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorSites::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать сайт",
                    'content' => $this->renderAjax('_form-sites', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать сайт",
                        'content' => $this->renderAjax('_form-sites', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-sites', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление сайта спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionSitesDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorSites */
        $model = SponsorSites::findOne($id);
        $model->is_deleted = 1;
        $model->save();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // пост ****************************************************************

    /**
     * Список постов спонсора
     *
     * @param $id
     * @return string
     */
    public function actionPost($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_post', [
            'post_date' => 'on',
            'title' => 'on',
            'status' => 'on',
            'assign_sponsor_id' => 'on',
            'post' => 'on',
        ]);
        $searchModel = new SponsorsPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('post', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать пост спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPostCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorPost();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать пост",
                    'content' => $this->renderAjax('_form-post', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать пост",
                        'content' => $this->renderAjax('_form-post', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-post', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование поста спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPostUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorPost::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать пост",
                    'content' => $this->renderAjax('_form-post', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать пост",
                        'content' => $this->renderAjax('_form-post', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-post', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление поста спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPostDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorPost */
        $model = SponsorPost::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Доступа ****************************************************************

    /**
     * Список доступов спонсора
     *
     * @param $id
     * @return string
     */
    public function actionAccess($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_access', [
            'sponsor_site_id' => 'on',
            'login' => 'on',
            'password' => 'off',
            'actual_date' => 'on',
            'status' => 'on',
            'comment' => 'on',

        ]);
        $searchModel = new SponsorsAccessSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('access', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать доступа спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionAccessCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorAccess();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать доступ",
                    'content' => $this->renderAjax('_form-access', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать доступ",
                        'content' => $this->renderAjax('_form-access', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-access', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование доступа спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionAccessUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorAccess::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать доступ",
                    'content' => $this->renderAjax('_form-access', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать доступ",
                        'content' => $this->renderAjax('_form-access', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-access', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление доступа спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionAccessDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorAccess */
        $model = SponsorAccess::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Backlinks ****************************************************************

    /**
     * Список Backlinks спонсора
     *
     * @param $id
     * @return string
     */
    public function actionBacklinks($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_backlinks', [
            'resource_page' => 'on',
            'period_scan' => 'on',
            'backlink_status' => 'on',
            'url' => 'on',
            'link_status' => 'on',
            'first_detection' => 'on',
            'last_detection' => 'on',

        ]);
        $searchModel = new SponsorsBacklinksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('backlinks', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать Backlinks спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionBacklinksCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorBacklinks();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать Backlinks",
                    'content' => $this->renderAjax('_form-backlinks', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать Backlinks",
                        'content' => $this->renderAjax('_form-backlinks', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-backlinks', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование Backlinks спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionBacklinksUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorBacklinks::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать Backlinks",
                    'content' => $this->renderAjax('_form-backlinks', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать Backlinks",
                        'content' => $this->renderAjax('_form-backlinks', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-access', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление Backlinks спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionBacklinksDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorAccess */
        $model = SponsorBacklinks::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // рейтинг ****************************************************************

    /**
     * Список рейтингов спонсора
     *
     * @param $sponsor_id
     * @return string
     */
    public function actionReview($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_review', [
            'param' => 'on',
            'value' => 'on',
            'score' => 'on',
        ]);
        $model = Sponsors::find()->where(['id' => $sponsor_id])->one();
        $searchModel = new SponsorsReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('review', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
            'model' => $model
        ]);
    }

    /**
     * Создать рейтинг спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionReviewCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorsReview();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать рейтинг",
                    'content' => $this->renderAjax('_form-review', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать рейтинг",
                        'content' => $this->renderAjax('_form-review', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-review', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование рейтинга спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionReviewUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorsReview::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать рейтинг",
                    'content' => $this->renderAjax('_form-review', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать рейтинг",
                        'content' => $this->renderAjax('_form-review', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-review', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление рейтинга спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionReviewDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorsReview */
        $model = SponsorsReview::findOne($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    // Заказы обзоров ****************************************************************

    /**
     * Список Заказы обзоров спонсора
     *
     * @param $sponsor_id
     * @return string
     */
    public function actionOrderReview($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_order_review', [
            'status' => 'on',
            'order_date' => 'on',
            'e_mail' => 'on',
            'order_sponsor_id' => 'on',
            'order_url' => 'on',
            'recip_URL' => 'on',
            'editor' => 'on',
            'writer' => 'on',
            'pay_amount' => 'on',
            'pay_status' => 'on',
        ]);
        $searchModel = new SponsorOrderReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('order-review', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать заказ обзора спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionOrderReviewCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorOrderReview();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать заказ обзоров",
                    'content' => $this->renderAjax('_form-order-review', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать заказ обзоров",
                        'content' => $this->renderAjax('_form-order-review', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-order-review', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование заказа обзора спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionOrderReviewUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorOrderReview::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать заказ обзоров",
                    'content' => $this->renderAjax('_form-order-review', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать заказ обзоров",
                        'content' => $this->renderAjax('_form-order-review', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-order-review', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление заказа обзоров спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionOrderReviewDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorOrderReview */
        $model = SponsorOrderReview::findOne($id);
        $model->delete();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    // Акции ****************************************************************

    /**
     * Список акций спонсора
     *
     * @param $sponsor_id
     * @return string
     */
    public function actionPromo($sponsor_id)
    {
        $sponsor_id = (int)$sponsor_id;
        $filter = ColumnVisible::getColumnVisible('sponsor_promo', [
            'name' => 'on',
            'image' => 'on',
            'content' => 'on',
            'start_date' => 'on',
            'end_date' => 'on',
            'url' => 'on',
            'sites' => 'on'
        ]);
        $searchModel = new SponsorPromoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sponsor_id);
        return $this->render('promo', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
            'sponsor_id' => $sponsor_id,
        ]);
    }

    /**
     * Создать акцию спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPromoCreate($sponsor_id)
    {
        $request = Yii::$app->request;
        $model = new SponsorPromo();
        $model->sponsor_id = (int)$sponsor_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать акцию",
                    'content' => $this->renderAjax('_form-promo', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                    if ($model->imageFile) {
                        $model->imageFile->saveAs("image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}");
                        $model->image = "/image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}";
                    }
                    $model->save();
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Создать акцию",
                        'content' => $this->renderAjax('_form-promo', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-promo', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * редактирование акцию спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPromoUpdate($id)
    {
        $request = Yii::$app->request;
        $model = SponsorPromo::find()->where(['id' => $id])->one();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать акцию",
                    'content' => $this->renderAjax('_form-promo', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    Yii::warning($model->sites);
                    $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                    if ($model->imageFile) {
                        $model->imageFile->saveAs("image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}");
                        $model->image = "/image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}";
                    }
                    $model->save();
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                } else {
                    return [
                        'title' => "Редактировать акциюв",
                        'content' => $this->renderAjax('_form-promo', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-promo', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * удаление акцию спонсора
     *
     * @param $id
     * @return array|string|Response
     */
    public function actionPromoDelete($id)
    {
        $request = Yii::$app->request;
        /* @var $model SponsorPromo */
        $model = SponsorPromo::findOne($id);
        $model->is_deleted = 1;
        $model->save();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Удаление пользователя
     *
     * @param $id
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $user = SponsorAccess::findOne($id);
        $user->is_deleted = 1;
        $user->save();
        return $this->redirect(['index']);
    }

    public function actionAdvertising($sponsor_id)
    {
        $model = Sponsors::findOne(intval($sponsor_id));

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Запись успешно сохранена');
            } else {
                var_dump($model->errors);
            }

        }
        return $this->render('advertising', ['model' => $model, 'sponsor_id' => $sponsor_id]);
    }

    public function actionSaveReviewText()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = (int)Yii::$app->request->post('sponsor');
        $sponsor = Sponsors::findOne($id);
        $sponsor->review_text = Yii::$app->request->post('text');
        $sponsor->save();
        return $sponsor->errors;
    }

    public function actionGetSponsorSites()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = (int)Yii::$app->request->post('sponsor');
        return SponsorSites::getSponsorSites($id);
    }

}
