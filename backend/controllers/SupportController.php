<?php

namespace backend\controllers;

use backend\models\search\SupportSearch;
use common\helpers\ColumnVisible;
use common\models\entity\Support;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller
 */
class SupportController extends Controller
{
    private $columnVisible = [
        'name' => 'on',
        'slug' => 'on',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница отображения списка пользователей
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter = ColumnVisible::getColumnVisible('support', $this->columnVisible);
        $searchModel = new SupportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
        ]);
    }

    /**
     * Создание пользователя
     *
     * @return string
     */
    public function actionCreate()
    {
        $model = new Support();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(['index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование пользователя
     *
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        $model = Support::findOne(intval($id));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись успешно сохранена');
            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Удаление пользователя
     *
     * @param $id
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $user = Support::findOne($id);
        $user->is_deleted = 1;
        $user->save();
        return $this->redirect(['index']);
    }
}
