<?php

namespace backend\controllers;

use backend\models\search\CategorySitesSearch;
use backend\models\search\TagsSearch;
use common\helpers\ColumnVisible;
use common\models\entity\CategorySites;
use common\models\entity\Tags;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class TagsController extends Controller
{
	private $columnVisible = [
		'name' => 'on',
		'slug' => 'on',
		'description' => 'on',
		'image' => 'on',
		'parent_id' => 'on',
		'is_visible' => 'on',
	];

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => ['logout', 'index', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Страница отображения списка пользователей
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		$filter = ColumnVisible::getColumnVisible('payment_systems', $this->columnVisible);
		$searchModel = new TagsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'filter' => $filter,
			'per_page' => Yii::$app->request->get('per-page', 20),
		]);
	}

	/**
	 * Создание пользователя
	 *
	 * @return string
	 */
	public function actionCreate()
	{
		$model = new Tags();
		if ($model->load(Yii::$app->request->post())) {
			$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
			if ($model->imageFile) {
				$model->imageFile->saveAs("image/tags/{$model->imageFile->baseName}.{$model->imageFile->extension}");
				$model->image = "/image/tags/{$model->imageFile->baseName}.{$model->imageFile->extension}";
			}
			if ($model->is_group==1) {
				$model->parent_id = 0;
			}
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'Запись успешно создана');
				return $this->redirect(['index']);
			}
			var_dump($model->errors);
		}
		return $this->render('create', ['model' => $model]);
	}

	/**
	 * Редактирование пользователя
	 *
	 * @param $id
	 *
	 * @return string
	 */
	public function actionUpdate($id)
	{
		$model = Tags::findOne(intval($id));
		$model->imageFile = $model->image;
		if ($model->parent_id == 0 ) {
			$model->is_group = 1;
		}
		if ($model->load(Yii::$app->request->post())) {
			$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
			if ($model->imageFile) {
				$model->imageFile->saveAs("image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}");
				$model->image = "/image/payment-system/{$model->imageFile->baseName}.{$model->imageFile->extension}";
			}
			if ($model->is_group==1) {
				$model->parent_id = 0;
			}
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'Запись успешно сохранена');
				return $this->redirect(['index']);
			}
			var_dump($model->errors);
		}
		return $this->render('update', ['model' => $model]);
	}

	/**
	 * Удаление пользователя
	 *
	 * @param $id
	 *
	 * @throws \Throwable
	 * @throws StaleObjectException
	 */
	public function actionDelete($id)
	{
		$tags = Tags::findOne($id);
		$tags->delete();
		return $this->redirect(['index']);
	}
}
