<?php

namespace backend\controllers;

use backend\models\search\SponsorsBacklinksSearch;
use common\helpers\ColumnVisible;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * PaymentsSales controller
 */
class BacklinkController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница отображения списка продаж
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter = ColumnVisible::getColumnVisible('sponsor_backlinks', [
            'sponsor_id' => 'on',
            'resource_page' => 'on',
            'period_scan' => 'on',
            'backlink_status' => 'on',
            'url' => 'on',
            'link_status' => 'on',
            'first_detection' => 'on',
            'last_detection' => 'on',
        ]);
        $searchModel = new SponsorsBacklinksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'filter' => $filter,
            'per_page' => Yii::$app->request->get('per-page', 20),
        ]);
    }

}
