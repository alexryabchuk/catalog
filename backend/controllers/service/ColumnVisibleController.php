<?php

namespace backend\controllers\service;

use common\helpers\ColumnVisible;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Site controller
 */
class ColumnVisibleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        $columnVisible = Yii::$app->request->post('filter');
        parse_str(Yii::$app->request->post('columnVisible'), $params);
        ColumnVisible::setColumnVisible(Yii::$app->request->post('tablename'), $columnVisible, $params);
        Yii::warning($params);
        return 'done';
    }

}
