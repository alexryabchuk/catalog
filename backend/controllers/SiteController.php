<?php
namespace backend\controllers;

use common\models\entity\CategorySites;
use common\models\entity\Template;
use common\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','index2','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = Template::findOne(1);
        /* @var $model Template */

        $fp = fopen( Yii::getAlias("@template/file.php"), "w");
        fwrite($fp, $model->template_php);
        fclose($fp);

        return $this->render("@template/file", [
            'model' => $model
        ]);
    }

    public function actionIndex2()
    {
        return $this->render("index");
    }

    public function actionView($sponsor_id)
    {
        $model = Template::findOne(3);
        /* @var $model Template */

        $fp = fopen( Yii::getAlias("@template/file.php"), "w");
        fwrite($fp, $model->template_php);
        fclose($fp);

        return $this->render("@template/file", [
            'id'=>$sponsor_id
        ]);

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
