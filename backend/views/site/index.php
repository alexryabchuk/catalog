<?php

/* @var $this yii\web\View */

// Подключения сервисного. Нужно для использования получения данних
use backend\models\service\SponsorsService;
use common\models\enums\PaymentsProgramTypes;

// Создание екземляка класса
$sponsorsService = new SponsorsService();
// Получение ТОП-спонсоров
$topSponsors = $sponsorsService->topSponsors(3);
// Получение Последних добавлених спонсоров
$lastSponsors = $sponsorsService->lastSponsors(5);
// Получение Последних добавлених постов
$lastPosts = $sponsorsService->lastPosts(10);

$this->title = 'Каталог партнерских программ';
?>

<style>
    .site-title {
        text-align: center;
        font-size: 24px;
        font-weight: bold;
    }

    .site-header {
        text-align: center;
        border: #0a0a0a solid 1px;
        padding: 20px;

    }

    .menu {
        margin-top: 20px;
        width: 100%;
        display: table;
        border-radius: 5px;
        border: #999999 solid 1px;
    }

    .menu ul {
        display: table-row;
    }

    .menu li {
        display: table-cell;
    }

    .menu ul li:hover, .menu a:hover {
        color: #999;
    }

    .menu li a {
        display: block;
        padding: 8px 15px;
        color: #000;
        text-align: center;
        text-decoration: underline;
    }
    .programm {
        border-radius: 5px;
        border: #999999 solid 2px;
        box-shadow: black;
        padding: 5px;
        margin-top: 10px;
    }

    .programm-image {
        float: left;
    }
    .programm-image img {
        max-width: 200px;
    }
    .programm-content {
        margin-left: 210px;
    }
</style>
<div class="site-index">
    <div class="row">
        <div class="col-md-9">
            <h1 class="site-title">Best Audit Affiliates Programm Reviews</h1>
        </div>
        <div class="col-md-3">
            <div class="menu">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/">Top Programms</a></li>
                    <li><a href="/">Sites </a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="about">
                <h3>About BAAPR</h3>
                <p>The best affiliate programs in the adult industry are the most honest and unbiased directory with up
                    to date information.</p>
                <p>We have been working in this business for over 15 years and are as immersed in the process as
                    possible - we have dozens of completed projects, several currently active sites and many plans for
                    the future. When we say BEST it means that we follow certain principles in selection. What is the
                    most important thing for a webmaster when working with an affiliate program? Of course, this is
                    profit, but not only. What can you say about honesty, politeness and adequacy of support, variety in
                    products or payment terms?</p>
                <p> We have combined all knowledge and factors into a rating system - yes, this is our subjective
                    rating, but it is based on many years of experience in cooperation with more than 500 programs. We
                    are honest with ourselves and transparently show you why we think this or that affiliate program is
                    good or bad.</p>
                <p> In addition to the directory of affiliate programs with ratings and reviews, you will find news -
                    sites that have opened or closed, updates in payments, promotions and others. If you are the owner
                    of an affiliate program - did not find it in the list, or do not agree with the rating - write to us
                    through the contact form.
                </p>
            </div>
            <div class="last-post">
                <h3>Programms updates</h3>
                <table width="100%">
                    <thead>
                    <th width="15%"></th>
                    <th width="30%"></th>
                    <th></th>
                    </thead>
                    <?php foreach ($lastPosts as $post) : ?>
                        <tr>
                            <td><?= $post->post_date ?></td>
                            <td><?= $post->sponsor->name ?></td>
                            <td><?= $post->title ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="top-programms">
                <h3>Top Programs</h3>
                <?php foreach ($topSponsors as $programm) : ?>
                    <div class="programm">
                        <div class="programm-image" >
                            <img src="<?=$programm->image?>">
                        </div>
                        <div class="programm-content" >
                            <div class="pp-name">
                                <h4>
                                    <?=$programm->name?>
                                    <span class="pull-right">89%</span>
                                </h4>
                            </div>
                            <div class="pp-sites">Sities: <?=count($programm->sites)?></div>
                            <div class="pp-programm">
                                programms:
                                <ul>
                                    <?php foreach ($programm->sponsorPaymentsPrograms as $p) :?>
                                        <li><?= PaymentsProgramTypes::getLabel($p->type)?>: <?=$p->sum_percent?></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>


                        </div>
                        <div class="clearfix"></div>
                        <div class="programm-description">
                            <div  class="pull-right" ><a href="/site/view?sponsor_id=<?=$programm->id?>" class="btn-sm btn-primary">Read Info</a></div>
                            <div><?= $programm->description ?></div>



                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="last-programms">
                <h3>Last added sponsors</h3>
                <?php foreach ($topSponsors as $programm) : ?>
                    <div class="programm">
                        <div class="programm-image" >
                            <img src="<?=$programm->image?>">
                        </div>
                        <div class="programm-content" >
                            <div class="pp-name">
                                <h4>
                                    <?=$programm->name?>
                                    <span class="pull-right">89%</span>
                                </h4>
                            </div>
                            <div class="pp-sites">Sities: <?=count($programm->sites)?></div>
                            <div class="pp-programm">
                                programms:
                                <ul>
                                <?php foreach ($programm->sponsorPaymentsPrograms as $p) :?>
                                    <li><?= PaymentsProgramTypes::getLabel($p->type)?>: <?=$p->sum_percent?></li>
                                <?php endforeach;?>
                                </ul>
                            </div>


                        </div>
                        <div class="clearfix"></div>
                        <div class="programm-description">
                            <div  class="pull-right" ><a href="/site/view?sponsor_id=<?=$programm->id?>" class="btn-sm btn-primary">Read Info</a></div>
                            <div><?= $programm->description ?></div>



                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
