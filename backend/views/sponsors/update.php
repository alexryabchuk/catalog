<?php

/* @var $this yii\web\View */

/* @var $model Sponsors */

use common\models\entity\Sponsors;

$this->title = 'Редактировать спонсора: '.$model->name;
?>
<div class="box">
    <div class="box-header">
        <h3><?=$this->title?></h3>
        <?= \common\widgets\SponsorNavBar::widget(['active' => 'update', 'id' => $model->id]) ?>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
