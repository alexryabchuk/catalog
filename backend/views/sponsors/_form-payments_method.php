<?php

use common\models\entity\PaymentSystems;
use common\models\entity\Sponsors;
use common\models\enums\PaymentsMethodStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Sponsors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model,
                'payment_system_id')->dropDownList(PaymentSystems::getPaymentSystemsList()) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'min_pay')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'fee')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'hold')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model,
                'status')->dropDownList(PaymentsMethodStatus::listData()) ?></div>
    </div>
    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
