<?php

use common\models\entity\SponsorPaymentsHistory;
use common\models\enums\PaymentsProgramTypes;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorPaymentsHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model,
                'type')->dropDownList(PaymentsProgramTypes::listData()) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'sum_percent')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'comment')->textarea() ?></div>
    </div>
    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
