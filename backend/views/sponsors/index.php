<?php

/* @var $this yii\web\View
 * @var $searchModel PaymentSystemSearch
 * @var $dataProvider ActiveDataProvider
 * @var $filter array
 * @var $per_page string
 */

use backend\models\search\PaymentSystemSearch;
use backend\models\search\SponsorsSearch;
use common\models\entity\CMSSponsor;
use common\models\entity\GeneralCompany;
use common\widgets\ColumnSetting;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Список спонсоров';
?>
<div class="payment-system-index">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                            class="fa fa-filter "></i></button>
                <button type="button" class="btn btn-box-tool" data-toggle="modal"
                        data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
            <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
            <div class="row" id="filter-panel">
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'name')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'slug')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'ref_url')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'domain')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'description')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'publication_status')->dropDownList([
                        null => 'Все',
                        '0' => 'Не активен',
                        '1' => 'Активен'
                    ]); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'recommendation_status')->dropDownList([
                        null => 'Все',
                        '0' => 'Не рекомендован',
                        '1' => 'Рекомендован'
                    ]); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'general_company_id')->dropDownList(arrayHelper::merge(
                        GeneralCompany::getGeneralCompanyList(), [null => 'Все'])); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'cms_id')->dropDownList(arrayHelper::merge(
                        [null => 'Все'], CMSSponsor::getCMSSponsorList())); ?>
                </div>
                <div class="col-md-2 col-md-offset-4">
                    <p style="margin-bottom: 5px;">&nbsp</p>
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'panelFooterTemplate' => '<div class="kv-panel-pager" style="float: left">{pager}</div>{footer}<div class="clearfix"></div>',
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->is_deleted) {
                        return ['style' => 'color:red'];
                    }
                    return [];
                },
                'panel' => [
                    'heading' => '<div>' .
                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                            ['title' => 'Добавить', 'class' => 'btn btn-info']) .
                        '</div>',
                    'before' => false,
                    'after' => '',
                    'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                        Html::a(10, Url::current(['per-page' => 10]),
                            ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(20, Url::current(['per-page' => 20]),
                            ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(50, Url::current(['per-page' => 50]),
                            ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                        '</div></div>',
                ],

                'columns' => [
                    [
                        'attribute' => 'name',
                        'visible' => $filter['name'] == 'on',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a($data->name, ['/sponsors/update', 'id' => $data->id]);
                        }
                    ],
                    [
                        'attribute' => 'slug',
                        'visible' => $filter['slug'] == 'on',
                    ],
                    [
                        'attribute' => 'ref_url',
                        'visible' => $filter['ref_url'] == 'on',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data PaymentSystemSearch */
                            if (strlen($data->ref_url) > 24) {
                                return "<span title='" . $data->ref_url . "'>" . substr($data->ref_url, 0,
                                        24) . "...</span>";
                            }
                            return $data->ref_url;
                        }

                    ],
                    [
                        'attribute' => 'domain',
                        'visible' => $filter['domain'] == 'on',
                    ],
                    [
                        'attribute' => 'description',
                        'visible' => $filter['description'] == 'on',
                        'contentOptions' => [
                            'style' => 'max - width:150px; overflow: auto; white - space: normal; word - wrap: break-word;'
                        ],
                        'value' => function ($data) {
                            /* @var $data PaymentSystemSearch */
                            return $data->description;
                        }
                    ],
                    [
                        'attribute' => 'publication_status',
                        'label' => 'Статус <br> публикации',
                        'encodeLabel' => false,
                        'visible' => $filter['publication_status'] == 'on',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data SponsorsSearch */
                            return $data->publication_status == 0 ? ' <i style = "color: red" class="glyphicon glyphicon-remove" ></i > ' : '<i style = "color: green" class="glyphicon glyphicon-ok" ></i > ';
                        }
                    ],
                    [
                        'attribute' => 'recommendation_status',
                        'label' => 'Статус <br> рекомендации',
                        'encodeLabel' => false,
                        'visible' => $filter['recommendation_status'] == 'on',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data SponsorsSearch */
                            return $data->recommendation_status == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove" ></i > ' : '<i style = "color: green" class="glyphicon glyphicon-ok" ></i > ';
                        }

                    ],
                    [
                        'attribute' => 'general_company_id',
                        'visible' => $filter['general_company_id'] == 'on',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data SponsorsSearch */
                            return $data->general_company_id ? $data->generalCompany->name : 'Отсутствует';
                        }

                    ],
                    [
                        'attribute' => 'cms_id',
                        'visible' => $filter['cms_id'] == 'on',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data SponsorsSearch */
                            return $data->cms->name ?? 'Не задано';
                        }

                    ],
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'visible' => $filter['image'] == 'on',
                        'value' => function ($data) {
                            /* @var $data PaymentSystemSearch */
                            return Html::img($data->image, ['style' => 'max-height:100px']);
                        }
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update} {delete}',
                    ]
                ],
            ]) ?>
        </div>
    </div>
</div>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'sponsors', 'model' => $searchModel]); ?>

