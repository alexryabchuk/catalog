<?php

use common\models\entity\SponsorAccess;
use common\models\entity\SponsorSites;
use common\models\enums\AccessStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12"><?= $form->field($model,
                'sponsor_site_id')->dropDownList(SponsorSites::getSponsorSites($model->sponsor_id)) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'login')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'password')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'actual_date')->textInput(['type' => 'date']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'status')->dropDownList(AccessStatus::listData()) ?></div>
    </div>

    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?></div>
    </div>


    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
