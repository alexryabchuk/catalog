<?php

/* @var $this yii\web\View */

/* @var $searchModel SponsorsPaymentsMethodSearch
 * @var $dataProvider ActiveDataProvider
 * @var $sponsor_id integer
 * @var $per_page integer
 * @var $filter array
 */


use backend\models\search\SponsorsPaymentsMethodSearch;
use common\models\entity\CategorySites;
use common\models\entity\Sponsors;
use common\widgets\ColumnSetting;
use common\widgets\SponsorNavBar;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$name = Sponsors::findOne($sponsor_id);
$this->title = 'Сайты: ' . $name->name;
$categoryList = CategorySites::getCategoryList();
$categoryList = ArrayHelper::merge([null => 'Без категории'], $categoryList);
CrudAsset::register($this);
?>
<div class="box">
    <div class="box-header">
        <h3><?= $this->title ?></h3>
        <?= SponsorNavBar::widget(['active' => 'sites', 'id' => $sponsor_id]) ?>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                    <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                                class="fa fa-filter "></i></button>
                    <button type="button" class="btn btn-box-tool" data-toggle="modal"
                            data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
                <input type="hidden" name="sponsor_id" value="<?= $sponsor_id ?>">
                <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
                <div class="row" id="filter-panel">
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'name')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'pps_status')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'main_category')->dropDownList($categoryList); ?>
                    </div>
                    <div class="col-md-2 col-md-offset-4">
                        <p style="margin-bottom: 5px;">&nbsp</p>
                        <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
                <div id="ajaxCrudDatatable">
                    <?= GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'rowOptions' => function ($model, $key, $index, $grid) {
                            if ($model->is_deleted) {
                                return ['style' => 'color:red'];
                            }
                            return [];
                        },
                        'pjax' => true,
                        'columns' => [
                            [
                                'attribute' => 'name',
                                'visible' => $filter['name'] == 'on',
                                'filter' => false,
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return '<a href="/' . $data->name . '" >' . $data->name . '</a>';
                                }
                            ],
                            [
                                'attribute' => 'pps_status',
                                'visible' => $filter['pps_status'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    return $data->pps_status == 1 ? 'Ok' : 'None';
                                }
                            ],
                            [
                                'attribute' => 'main_category',
                                'visible' => $filter['main_category'] == 'on',
                                'filter' => false,
                                'value' => function ($data) use ($categoryList) {
                                    return $categoryList[$data->main_category] ?? 'Без категории';
                                }
                            ],
                            [
                                'label' => 'Дополнительние категории',
                                'visible' => $filter['add_category'] == 'on',
                                'format' => 'raw',
                                'filter' => false,
                                'value' => function ($data) use ($categoryList) {
                                    $addCategory = '';
                                    if (isset($categoryList[$data->category1])) {
                                        $addCategory .= $categoryList[$data->category1] . '<br>';
                                    }
                                    if (isset($categoryList[$data->category2])) {
                                        $addCategory .= $categoryList[$data->category2] . '<br>';
                                    }
                                    if (isset($categoryList[$data->category3])) {
                                        $addCategory .= $categoryList[$data->category3] . '<br>';
                                    }
                                    if (isset($categoryList[$data->category4])) {
                                        $addCategory .= $categoryList[$data->category4] . '<br>';
                                    }
                                    if (isset($categoryList[$data->category5])) {
                                        $addCategory .= $categoryList[$data->category5] . '<br>';
                                    }
                                    return $addCategory;
                                }
                            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'dropdown' => false,
                                'vAlign' => 'middle',
                                'urlCreator' => function ($action, $model, $key, $index) use ($sponsor_id) {
                                    return Url::to([
                                        'sites-' . $action,
                                        'id' => $key
                                    ]);
                                },
                                'width' => '120px',
                                'template' => '{update}{delete}',
                                'updateOptions' => [
                                    'label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
                                    'role' => 'modal-remote',
                                    'title' => 'Изменить',
                                    'data-toggle' => 'tooltip'
                                ],
                                'deleteOptions' => [
                                    'role' => 'modal-remote',
                                    'title' => 'Удалить',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Вы уверенны?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить запись'
                                ],
                            ],

                        ],

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'heading' => '<div>' .
                                Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>',
                                    ['sites-create', 'sponsor_id' => $sponsor_id],
                                    ['title' => 'Добавить', 'role' => 'modal-remote', 'class' => 'btn btn-info']) .
                                '</div>',
                            'before' => false,
                            'after' => '',
                            'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                                Html::a(10, Url::current(['per-page' => 10]),
                                    ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(20, Url::current(['per-page' => 20]),
                                    ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(50, Url::current(['per-page' => 50]),
                                    ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                                '</div></div>',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'sponsor_sites', 'model' => $searchModel]); ?>
