<?php

use common\models\entity\SponsorPost;
use common\models\entity\Sponsors;
use common\models\entity\SponsorSites;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorPost */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="payment-system-form">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model,
                    'post_date')->textInput(['type' => 'date']) ?></div>
            <div class="col-md-6"><?= $form->field($model,
                    'status')->dropDownList([0 => 'Черновик', 1 => 'Опубликован']) ?></div>
        </div>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model,
                    'assign_sponsor_id')->dropDownList(Sponsors::getSponsorList()) ?></div>
            <div class="col-md-6">
                <?= $model->isNewRecord ?
                    $form->field($model, 'assign_site_id')->dropDownList([]) :
                    $form->field($model,
                        'assign_site_id')->dropDownList(SponsorSites::getSponsorSites($model->assign_sponsor_id))
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"><?= $form->field($model,
                    'title')->textInput() ?></div>
        </div>
        <div class="row">
            <div class="col-md-12"><?= $form->field($model,
                    'post')->textarea() ?></div>
        </div>
        <?php if (!Yii::$app->request->isAjax): ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                    ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
            </div>
        <?php endif; ?>
        <?php ActiveForm::end(); ?>
    </div>
<?php
$new = $model->isNewRecord ? 1 : 2;
$script = <<<JS
function updateSiteList() {
    $.ajax({
        url: '/sponsors/get-sponsor-sites',
        type: 'post',
        dataType: 'json',
        data: {
            sponsor: $('#sponsorpost-assign_sponsor_id').val()
        },
        success: function (response) {
            console.log(response);
            let selSite = $('#sponsorpost-assign_site_id');
            selSite.empty();
            $.each(response, function (key, value) {
                selSite.append('<option value=' + key + '>'
                    + value + '</option>');
            });
            
        }
    });
};
$('#sponsorpost-assign_sponsor_id').on('change', function (){
    updateSiteList();
});
if ($new == 1) {
    updateSiteList();
};
JS;
$this->registerJs($script);
?>