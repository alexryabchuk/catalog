<?php

use common\models\entity\CMSSponsor;
use common\models\entity\GeneralCompany;
use common\models\entity\Sponsors;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Sponsors */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="payment-system-form">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-md-6"><?= $form->field($model, 'slug', [
                    'addon' => [
                        'append' => [
                            'content' => Html::button('<i class="glyphicon glyphicon-refresh"></i>',
                                ['class' => 'btn btn-default', 'id' => 'generate-slug']),
                            'asButton' => true
                        ]
                    ]
                ])->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ref_url')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-1" style="padding-top:35px ">
                <?= Html::a('<i class="fa fa-external-link"></i>Перейти', ['/visit/' . $model->ref_url]) ?>
            </div>
            <div class="col-md-1" style="padding-top:35px ">
                <?= Html::a('<i class="fa fa-exclamation-circle"></i>', ['#'],
                    [
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "Дата сканирования:" . $model->ref_url_date_scan . " Ответ:" . $model->ref_url_status_scan
                    ]) ?>
                <span class="btn-success">200 OK</span>
                <?= Html::a('<i class="fa fa-refresh"></i>', ['/visit/' . $model->ref_url]) ?>

            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'domain')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-1" style="padding-top:35px ">
                <?= Html::a('<i class="fa fa-exclamation-circle"></i>', ['#'],
                    [
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "Дата регистрации домена"
                    ]) ?>

            </div>
        </div>


        <?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'publication_status')->checkbox() ?></div>
            <div class="col-md-6"><?= $form->field($model, 'recommendation_status')->checkbox() ?></div>
        </div>
        <div class="row not-recommendation-reason"
             style="display: <?= !$model->recommendation_status ? 'block' : 'none' ?>">
            <div class="col-md-12">
                <?= $form->field($model, 'not_recommendation_reason')->textarea(['rows' => 3]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6"><?= $form->field($model,
                    'general_company_id')->dropDownList(GeneralCompany::getGeneralCompanyList()) ?></div>
            <div class="col-md-6"><?= $form->field($model,
                    'cms_id')->dropDownList(CMSSponsor::getCMSSponsorList(), ['prompt' => 'Нет']) ?></div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'imageFile')->widget(FileInput::class, [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
                        'initialPreviewAsData' => true,
                        'showPreview' => true,
                        'showRemove' => false,
                        'showUpload' => false
                    ],
                ]); ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                [
                    'class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning',
                    'formaction' => Url::to(['sponsors/'.Yii::$app->controller->action->id,'id'=>$model->id])
                ]) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Создать и Выйти' : 'Сохранить и Выйти',
                [
                    'class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning',
                    'formaction' => Url::to(['/sponsors/'.Yii::$app->controller->action->id, 'exit' => true,'id'=>$model->id]),
                ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php
$script = <<<JS
function generateSlug(){
    let sponsorsName = $("#sponsors-name").val();
    sponsorsName = sponsorsName.replaceAll(' ', '-');
    sponsorsName = sponsorsName.toLowerCase();
    $("#sponsors-slug").val(sponsorsName);
}
$("#generate-slug").on('click',function (){
   generateSlug(); 
});
$("#sponsors-name").on('change',function (){
   let sponsorsSlug = $("#sponsors-slug").val();
   
    if (sponsorsSlug == '') {
        generateSlug();
    } 
});
$('[data-toggle="tooltip"]').tooltip();
$('#sponsors-recommendation_status').parent().on('change',function (){
    if ($('#sponsors-recommendation_status').is(':checked')){
	$('.not-recommendation-reason').hide();
} else {
	$('.not-recommendation-reason').show();
}
})
JS;
$this->registerJs($script);
?>