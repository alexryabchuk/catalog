<?php

use common\models\entity\SponsorPromo;
use common\models\entity\SponsorSites;
use conquer\select2\Select2Widget;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorPromo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name')->textInput() ?></div>
                <div class="col-md-12"><?= $form->field($model, 'content') ?></div>
            </div>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'imageFile')->widget(FileInput::class, [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
                    'initialPreviewAsData' => true,
                    'showPreview' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'start_date')->textInput(['type' => 'date']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'end_date')->textInput(['type' => 'date']) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'url')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'sites')->widget(
                Select2Widget::class,
                [
                    'items' => SponsorSites::getSponsorSites($model->sponsor_id),
                    'multiple' => true,
                ]
            );
            ?></div>
    </div>


    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
