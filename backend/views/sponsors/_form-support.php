<?php

use common\models\entity\SponsorSupport;
use common\models\entity\Support;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorSupport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model,
                'support_id')->dropDownList(Support::getSupportList()) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'contact')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'response_time')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?></div>
    </div>

    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
