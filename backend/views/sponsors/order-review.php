<?php

/* @var $this yii\web\View */

/* @var $searchModel SponsorsPaymentsMethodSearch
 * @var $dataProvider ActiveDataProvider
 * @var $sponsor_id integer
 * @var $per_page integer
 * @var $filter array
 */


use backend\models\search\SponsorsPaymentsMethodSearch;
use common\models\entity\SponsorOrderReview;
use common\models\entity\Sponsors;
use common\models\enums\OrderReviewStatus;
use common\widgets\ColumnSetting;
use common\widgets\SponsorNavBar;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
$name = Sponsors::findOne($sponsor_id);
$this->title = 'Заказы обзоров: '. $name->name;

CrudAsset::register($this);
?>
<div class="box">
    <div class="box-header">
        <h3><?=$this->title?></h3>
        <?= SponsorNavBar::widget(['active' => 'order-review', 'id' => $sponsor_id]) ?>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                    <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                                class="fa fa-filter "></i></button>
                    <button type="button" class="btn btn-box-tool" data-toggle="modal"
                            data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
                <input type="hidden" name="sponsor_id" value="<?= $sponsor_id ?>">
                <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
                <div class="row" id="filter-panel">
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'status')->dropDownList(
                            ArrayHelper::merge([null => 'Все'], OrderReviewStatus::listData())
                        ); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'order_date')->textInput(['type' => 'date']); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'e_mail')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'recip_URL')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'editor')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'writer')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'pay_amount')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'pay_status')->dropDownList([
                            null => 'Все',
                            0 => 'Нет',
                            1 => 'Да'
                        ]); ?>
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        <p style="margin-bottom: 5px;">&nbsp</p>
                        <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
                <div id="ajaxCrudDatatable">
                    <?= GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'rowOptions' => function ($model, $key, $index, $grid) {
                            if ($model->is_deleted) {
                                return ['style' => 'color:red'];
                            }
                            return [];
                        },
                        'pjax' => true,
                        'columns' => [
                            [
                                'attribute' => 'order_date',
                                'visible' => $filter['order_date'] == 'on',
                                'filter' => false,
                                'format' => ['date', 'php:d.m.Y'],
                            ],
                            [
                                'attribute' => 'status',
                                'visible' => $filter['status'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    return OrderReviewStatus::getLabel($data->status);
                                }
                            ],
                            [
                                'attribute' => 'e_mail',
                                'visible' => $filter['e_mail'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'order_sponsor_id',
                                'label'=>'Спонсор/URL',
                                'visible' => $filter['order_sponsor_id'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    /* @var $data SponsorOrderReview */
                                    return $data->order_sponsor_id ? $data->orderSponsor->name : $data->order_url;
                                }
                            ],
                            [
                                'attribute' => 'recip_URL',
                                'visible' => $filter['recip_URL'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'editor',
                                'visible' => $filter['editor'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'writer',
                                'visible' => $filter['writer'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'pay_amount',
                                'visible' => $filter['pay_amount'] == 'on',
                                'filter' => false,
                                'format' => 'raw',
                                'value' => function ($data) {
                                    /* @var $data SponsorOrderReview */
                                    $status = $data->pay_status == 0 ? '<i class="fa fa-close" style="color: red"></i>' : '<i class="fa fa-check" style="color: green"></i>';
                                    return $status . ' ' . $data->pay_amount;
                                }
                            ],

                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'dropdown' => false,
                                'vAlign' => 'middle',
                                'urlCreator' => function ($action, $model, $key, $index) use ($sponsor_id) {
                                    return Url::to([
                                        'order-review-' . $action,
                                        'id' => $key
                                    ]);
                                },
                                'width' => '120px',
                                'template' => '{update}{delete}',
                                'updateOptions' => [
                                    'label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
                                    'role' => 'modal-remote',
                                    'title' => 'Изменить',
                                    'data-toggle' => 'tooltip'
                                ],
                                'deleteOptions' => [
                                    'role' => 'modal-remote',
                                    'title' => 'Удалить',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Вы уверенны?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить запись'
                                ],
                            ],

                        ],

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'heading' => '<div>' .
                                Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>',
                                    ['order-review-create', 'sponsor_id' => $sponsor_id],
                                    ['title' => 'Добавить', 'role' => 'modal-remote', 'class' => 'btn btn-info']) .
                                '</div>',
                            'before' => false,
                            'after' => '',
                            'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                                Html::a(10, Url::current(['per-page' => 10]),
                                    ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(20, Url::current(['per-page' => 20]),
                                    ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(50, Url::current(['per-page' => 50]),
                                    ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                                '</div></div>',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'sponsor_sites', 'model' => $searchModel]); ?>
