<?php

/* @var $this yii\web\View */

/* @var $searchModel SponsorsPaymentsMethodSearch
 * @var $dataProvider ActiveDataProvider
 * @var $sponsor_id integer
 * @var $per_page integer
 * @var $filter array
 */


use backend\models\search\SponsorsPaymentsMethodSearch;
use common\models\entity\PaymentSystems;
use common\models\entity\Sponsors;
use common\models\enums\PaymentsMethodStatus;
use common\widgets\ColumnSetting;
use common\widgets\SponsorNavBar;
use common\widgets\SponsorPaymentsNavBar;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
$name = Sponsors::findOne($sponsor_id);
$this->title = 'Методы платежей: ' . $name->name;

CrudAsset::register($this);
?>
<div class="box">
    <div class="box-header">
        <h3><?= $this->title ?></h3>
        <?= SponsorNavBar::widget(['active' => 'payments', 'id' => $sponsor_id]) ?>
    </div>
    <div class="box-body">
        <div class="box">
            <?= SponsorPaymentsNavBar::widget(['active' => 'payments-method', 'id' => $sponsor_id]) ?>
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                    <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                                class="fa fa-filter "></i></button>
                    <button type="button" class="btn btn-box-tool" data-toggle="modal"
                            data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
                <input type="hidden" name="sponsor_id" value="<?= $sponsor_id ?>">
                <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
                <div class="row" id="filter-panel">
                    <div class="col-md-2">
                        <?= $form->field($searchModel,
                            'payment_system_id')->dropDownList(arrayHelper::merge(
                            [null => 'Все'], PaymentSystems::getPaymentSystemsList())); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'min_pay')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'fee')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'hold')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'status')->dropDownList(arrayHelper::merge(
                            [null => 'Все'], PaymentsMethodStatus::listData())); ?>
                    </div>
                    <div class="col-md-2">
                        <p style="margin-bottom: 5px;">&nbsp</p>
                        <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
                <div id="ajaxCrudDatatable">
                    <?= GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax' => true,
                        'columns' => [
                            [
                                'attribute' => 'payment_system_id',
                                'visible' => $filter['payment_system_id'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    /* @var $data SponsorsPaymentsMethodSearch */
                                    return $data->paymentSystem->name;
                                }

                            ],
                            [
                                'attribute' => 'min_pay',
                                'visible' => $filter['min_pay'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'fee',
                                'visible' => $filter['fee'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'hold',
                                'visible' => $filter['hold'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'status',
                                'visible' => $filter['status'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    /* @var $data SponsorsPaymentsMethodSearch */
                                    return PaymentsMethodStatus::getLabel($data->status);
                                }
                            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'dropdown' => false,
                                'vAlign' => 'middle',
                                'urlCreator' => function ($action, $model, $key, $index) use ($sponsor_id) {
                                    return Url::to([
                                        'payments-method-' . $action,
                                        'id' => $key
                                    ]);
                                },
                                'width' => '120px',
                                'template' => '{update}{delete}',
                                'updateOptions' => [
                                    'label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
                                    'role' => 'modal-remote',
                                    'title' => 'Изменить',
                                    'data-toggle' => 'tooltip'
                                ],
                                'deleteOptions' => [
                                    'role' => 'modal-remote',
                                    'title' => 'Удалить',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Вы уверенны?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить запись'
                                ],
                            ],

                        ],

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'heading' => '<div>' .
                                Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>',
                                    ['payments-method-create', 'sponsor_id' => $sponsor_id],
                                    ['title' => 'Добавить', 'role' => 'modal-remote', 'class' => 'btn btn-info']) .
                                '</div>',
                            'before' => false,
                            'after' => '',
                            'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                                Html::a(10, Url::current(['per-page' => 10]),
                                    ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(20, Url::current(['per-page' => 20]),
                                    ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(50, Url::current(['per-page' => 50]),
                                    ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                                '</div></div>',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'payments_method', 'model' => $searchModel]); ?>
