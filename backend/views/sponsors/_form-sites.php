<?php

use common\models\entity\CategorySites;
use common\models\entity\SponsorSupport;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorSupport */
/* @var $form yii\widgets\ActiveForm */
$categoryList = CategorySites::getCategoryList();
$categoryList = \yii\helpers\ArrayHelper::merge([null => 'Без категории'], $categoryList);
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'name')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model,
                'pps_status')->dropDownList([1 => 'Ok', 2 => 'none']) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'main_category')->dropDownList($categoryList) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'category1')->dropDownList($categoryList) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'category2')->dropDownList($categoryList) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'category3')->dropDownList($categoryList) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'category4')->dropDownList($categoryList) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'category5')->dropDownList($categoryList) ?></div>
    </div>


    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
