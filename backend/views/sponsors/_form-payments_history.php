<?php

use common\models\entity\SponsorPaymentsHistory;
use common\models\entity\SponsorPaymentsMethod;
use common\models\enums\PaymentsStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorPaymentsHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model,
                'payments_method_id')->dropDownList(SponsorPaymentsMethod::getSponsorPaymentsMethodList($model->sponsor_id)) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'date_pay')->textInput(['type'=>'date']) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'sum_pay')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model,
                'status')->dropDownList(PaymentsStatus::listData()) ?></div>
    </div>
    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
