<?php

use common\models\entity\SponsorAccess;
use common\models\enums\BacklinkStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'resource_page')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'period_scan')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model,
                'backlink_status')->dropDownList(BacklinkStatus::listData()) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'link_status')->dropDownList([
                0 => 'Не активна',
                1 => 'Активна'
            ]); ?></div>
    </div>

    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'url')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'first_detection')->textInput(['type' => 'date']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'last_detection')->textInput(['type' => 'date']) ?></div>
    </div>

    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
