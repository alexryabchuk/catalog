<?php

use common\models\entity\SponsorAccess;
use common\models\entity\Sponsors;
use common\models\enums\OrderReviewStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SponsorAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'status')->dropDownList(OrderReviewStatus::listData()) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'order_date')->textInput(['type' => 'date']) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'e_mail')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'recip_URL')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model,
                'order_sponsor_id')->dropDownList(ArrayHelper::merge([null => 'Нет'],
                Sponsors::getSponsorList())) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'order_url')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'editor')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'writer')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'pay_status')->dropDownList([0 => 'нет', 1 => 'да']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'pay_amount')->textInput() ?></div>
    </div>


    <?php if (!Yii::$app->request->isAjax): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
