<?php

use common\models\entity\Sponsors;
use common\widgets\SponsorNavBar;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Sponsors */
/* @var $form yii\widgets\ActiveForm
 * @var $sponsor_id integer
 * @var $per_page integer
 * @var $filter array
 */

$name = Sponsors::findOne($sponsor_id);
$this->title = 'Рекламные материалы: '. $name->name;
?>

<div class="payment-system-form">
    <div class="box">
        <div class="box-header">
            <h3><?=$this->title?></h3>
            <?= SponsorNavBar::widget(['active' => 'advertising', 'id' => $sponsor_id]) ?>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_trafic')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_banners')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_fhg')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_hosted')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_downloadable')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_embedded')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'ad_models')->checkbox() ?></div>
            </div>


            <div class="form-group">
                <?= Html::submitButton('Сохранить',
                    ['class' => 'btn btn-info']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>