<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\PaymentSystems */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?= $this->title ?></h3>
	</div>
	<div class="box-body">
<div class="payment-system-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
		<div class="col-md-10">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-2" style="padding-top: 30px;">
			<?= $form->field($model, 'is_active')->checkbox() ?>
		</div>
	</div>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'description')->textarea(['rows' => 16]) ?>
		</div>
        <div class="col-md-6">
            <?= $form->field($model, 'imageFile')->widget(FileInput::class, [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreview' => ($model->isNewRecord || !$model->image) ? false :$model->image,
                    'initialPreviewAsData'=>true,
                    'showPreview' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ],
            ]); ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить',
            ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
	</div>
</div>