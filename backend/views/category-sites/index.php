<?php

/* @var $this yii\web\View
 * @var $searchModel CategorySites
 * @var $dataProvider ActiveDataProvider
 * @var $filter array
 * @var $per_page string
 */

use common\models\entity\CategorySites;
use common\widgets\ColumnSetting;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$this->title = 'Категории сайтов';
?>
<div class="payment-system-index">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                            class="fa fa-filter "></i></button>
                <button type="button" class="btn btn-box-tool" data-toggle="modal"
                        data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
            <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
            <div class="row" id="filter-panel">
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'name')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'slug')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'description')->textInput(); ?>
                </div>
				<div class="col-md-2">
					<?= $form->field($searchModel, 'is_active')->dropDownList([
						null => 'Все',
						'0' => 'Не активен',
						'1' => 'Активен'
					]); ?>
				</div>
                <div class="col-md-2 col-md-offset-2">
                    <p style="margin-bottom: 5px;">&nbsp</p>
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'panelFooterTemplate' => '<div class="kv-panel-pager" style="float: left">{pager}</div>{footer}<div class="clearfix"></div>',
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->is_deleted) {
                        return ['style' => 'color:red'];
                    }
                    return [];
                },
                'panel' => [
                    'heading' => '<div>' .
                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                            ['title' => 'Добавить', 'class' => 'btn btn-info']) .
                        '</div>',
                    'before' => false,
                    'after' => '',
                    'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                        Html::a(10, Url::current(['per-page' => 10]),
                            ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(20, Url::current(['per-page' => 20]),
                            ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(50, Url::current(['per-page' => 50]),
                            ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                        '</div></div>',
                ],

                'columns' => [
                    [
                        'attribute' => 'name',
                        'visible' => $filter['name'] == 'on',
                    ],
                    [
                        'attribute' => 'slug',
                        'visible' => $filter['slug'] == 'on',
                    ],
                    [
                        'attribute' => 'description',
                        'visible' => $filter['description'] == 'on',
                        'contentOptions' => [
                            'style'=>'max-width:150px; overflow: auto; white-space: normal; word-wrap: break-word;'
                        ],
                        'value' => function ($data) {
                            /* @var $data CategorySites */
                            return $data->description;
                        }
                    ],
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'visible' => $filter['image'] == 'on',
                        'value' => function ($data) {
                            /* @var $data CategorySites */
                            return Html::img('/'.$data->image,['style'=>'max-height:100px']);
                        }
                    ],
					[
						'attribute' => 'is_active',
						'visible' => $filter['is_active'] == 'on',
						'format' => 'raw',
						'value' => function ($data) {
							/* @var $data CategorySites */
							return $data->is_active == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' : '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
						}
					],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update} {delete}',
                    ]
                ],
            ]) ?>
        </div>
    </div>
</div>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'category_sites', 'model' => $searchModel]); ?>

