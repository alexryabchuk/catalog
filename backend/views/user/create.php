<?php

/* @var $this yii\web\View */
/* @var $model User */

use common\models\User;

$this->title = 'Создать пользователя';
?>
<div class="user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
