<?php

use common\models\enums\UserRole;
use common\models\enums\UserStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $model->isNewRecord ? $form->field($model,
        'password_hash')->textInput(['maxlength' => true]) : $form->field($model,
        'new_password')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList(UserStatus::listData()) ?>
    <?= $form->field($model, 'role')->dropDownList(UserRole::listData()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить',
            ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
