<?php

/* @var $this yii\web\View
 * @var $searchModel UserSearch
 * @var $dataProvider ActiveDataProvider
 * @var $filter array
 * @var $per_page string
 */

use backend\models\search\UserSearch;
use common\models\enums\UserRole;
use common\models\enums\UserStatus;
use common\widgets\ColumnSetting;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Список пользователей';
?>
<div class="user-index">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                            class="fa fa-filter "></i></button>
                <button type="button" class="btn btn-box-tool" data-toggle="modal"
                        data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
            <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
            <div class="row" id="filter-panel">
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'username')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'email')->textInput(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'role')->dropDownList(arrayHelper::merge(
                        [null => 'Все'], UserRole::listData())); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'status')->dropDownList(arrayHelper::merge(
                        [null => 'Все'], UserStatus::listData())); ?>
                </div>
                <div class="col-md-2 col-md-offset-2">
                    <p style="margin-bottom: 5px;">&nbsp</p>
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'panelFooterTemplate' => '<div class="kv-panel-pager" style="float: left">{pager}</div>{footer}<div class="clearfix"></div>',
                'panel' => [
                    'heading' => '<div>' .
                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                            ['title' => 'Добавить', 'class' => 'btn btn-info']) .
                        '</div>',
                    'before' => false,
                    'after' => '',
                    'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                        Html::a(10, Url::current(['per-page' => 10]),
                            ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(20, Url::current(['per-page' => 20]),
                            ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(50, Url::current(['per-page' => 50]),
                            ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                        '</div></div>',
                ],

                'columns' => [
                    [
                        'attribute' => 'username',
                        'visible' => $filter['username'] == 'on',
                    ],
                    [
                        'attribute' => 'email',
                        'visible' => $filter['email'] == 'on',
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'html',
                        'visible' => $filter['status'] == 'on',
                        'value' => function ($data) {
                            /* @var $data UserSearch */
                            return UserStatus::getLabel($data->status);
                        }
                    ],
                    [
                        'attribute' => 'role',
                        'format' => 'html',
                        'visible' => $filter['role'] == 'on',
                        'value' => function ($data) {
                            /* @var $data UserSearch */
                            return UserRole::getLabel($data->role);
                        }
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update} {delete}',
                    ]
                ],
            ]) ?>
        </div>
    </div>
</div>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'user', 'model' => $searchModel]); ?>

