<?php

use common\models\entity\Tags;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\PaymentSystems */
/* @var $form yii\widgets\ActiveForm */
$mainGroup = Tags::getMainGroup();
?>
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= $this->title ?></h3>
		</div>
		<div class="box-body">
			<div class="payment-system-form">
				<?php $form = ActiveForm::begin(); ?>
				<div class="row">
					<div class="col-md-10">
						<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-2" style="padding-top: 30px;">
						<?= $form->field($model, 'is_visible')->checkbox() ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10">
						<?= $form->field($model, 'parent_id')->dropDownList($mainGroup, [
							'disabled' => $model->is_group == 1
						]) ?>
					</div>
					<div class="col-md-2" style="padding-top: 30px;">
						<?= $form->field($model, 'is_group')->checkbox([
							'disabled' => $model->is_group == 1 && !$model->isNewRecord,
						]) ?>
					</div>
				</div>
				<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

				<div class="row">
					<div class="col-md-6">
						<?= $form->field($model, 'description')->textarea(['rows' => 15]) ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'imageFile')->widget(FileInput::class, [
							'options' => ['accept' => 'image/*'],
							'pluginOptions' => [
								'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
								'initialPreviewAsData' => true,
								'showPreview' => true,
								'showRemove' => false,
								'showUpload' => false
							],
						]); ?>
					</div>
				</div>


				<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить',
						['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>

<?php
$script = <<<JS
$('#tags-is_group').change(function () {
	$('#tags-parent_id').prop('disabled', function(i, v) { return !v; });    
});
JS;
$this->registerJs($script);
?>