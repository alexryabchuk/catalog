<?php
$controller = Yii::$app->controller->id;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    [
                        'label' => 'Партнерские программы',
                        'active' => in_array($controller,['general-company','sponsors','payments-sales','payments','post','access','backlink','order','promo']),
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список ПП', 'icon' => 'file-code-o', 'url' => '/sponsors','active' => in_array($controller,['sponsors'])],
                            ['label' => 'Обьединение компаний', 'icon' => 'dashboard', 'url' => '/general-company','active' => in_array($controller,['general-company'])],
                            ['label' => 'Продажи', 'icon' => 'dashboard', 'url' => '/payments-sales','active' => in_array($controller,['payments-sales'])],
                            ['label' => 'Платежи', 'icon' => 'dashboard', 'url' => '/payments','active' => in_array($controller,['payments'])],
                            ['label' => 'Запросы', 'icon' => 'dashboard', 'url' => '#',],
                            ['label' => 'Посты', 'icon' => 'dashboard', 'url' => '/post','active' => in_array($controller,['post'])],
                            ['label' => 'Доступы', 'icon' => 'dashboard', 'url' => '/access','active' => in_array($controller,['access'])],
                            ['label' => 'Backlinks', 'icon' => 'dashboard', 'url' => '/backlink','active' => in_array($controller,['backlink'])],
                            ['label' => 'Заказы', 'icon' => 'dashboard', 'url' => '/order','active' => in_array($controller,['order'])],
                            ['label' => 'Промо', 'icon' => 'dashboard', 'url' => '/promo','active' => in_array($controller,['promo'])],

                        ],
                    ],
                    [
                        'label' => 'Сайти',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список сайтов', 'icon' => 'file-code-o', 'url' => '/sites','active' => in_array($controller,['sites'])],
                        ],
                    ],
                    [
                        'label' => 'Справочники',
                        'icon' => 'share',
                        'active' => in_array($controller,['payment-system','support','cmssponsor','category-sites','tags']),
                        'url' => '#',
                        'items' => [
                            ['label' => 'Платежные системы', 'icon' => 'file-code-o', 'url' => '/payment-system','active' => in_array($controller,['payment-system'])],
                            ['label' => 'Виды супорта', 'icon' => 'file-code-o', 'url' => '/support','active' => in_array($controller,['support'])],
                            ['label' => 'CMS партнерок', 'icon' => 'file-code-o', 'url' => '/cmssponsor','active' => in_array($controller,['cmssponsor'])],
							['label' => 'Категории', 'icon' => 'file-code-o', 'url' => '/category-sites','active' => in_array($controller,['category-sites'])],
							['label' => 'Теги', 'icon' => 'file-code-o', 'url' => '/tags','active' => in_array($controller,['tags'])],
                        ],
                    ],
                    [
                        'label' => 'Настройки системи',
                        'icon' => 'share',
                        'active' => in_array($controller,['user','template']),
                        'url' => '#',
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'file-code-o', 'url' => '/user','active' => in_array($controller,['user']),],
                            ['label' => 'Шаблоны', 'icon' => 'file-code-o', 'url' => '/template','active' => in_array($controller,['template']),],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
