<?php

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 */

use common\models\entity\Template;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;

$this->title = 'Список шаблонов';
?>
<div class="user-index">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,

                'columns' => [
                    [
                        'attribute' => 'name',

                    ],
                    [
                        'attribute' => 'template_php',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data Template */
                            $default = $data->default_template == 1 ?
                                '<i style="color: green" class="btn glyphicon glyphicon-ok"></i>' :
                                Html::a('<i style="color: red" class="btn glyphicon glyphicon-remove"></i>',
                                    ['/template/set-default', 'id' => $data->id, 'default_template' => 1]);
                            $viewBtn =  Html::a('<i style="color: blue" class="btn glyphicon glyphicon-eye-open"></i>',
                                    ['/template/view-php', 'id' => $data->id]);
                            return Html::a('<span class="btn btn-primary">Редактировать</span>',
                                    ['/template/edit-php', 'id' => $data->id]) . $default.$viewBtn;
                        }
                    ],
                    [
                        'attribute' => 'template_smarty',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data Template */
                            $default = $data->default_template == 2 ?
                                '<i style="color: green" class="btn glyphicon glyphicon-ok"></i>' :
                                Html::a('<i style="color: red" class="btn glyphicon glyphicon-remove"></i>',
                                    ['/template/set-default', 'id' => $data->id, 'default_template' => 2]);
                            $viewBtn =  Html::a('<i style="color: blue" class="btn glyphicon glyphicon-eye-open"></i>',
                                ['/template/view-smarty', 'id' => $data->id]);
                            return Html::a('<span class="btn btn-primary">Редактировать</span>',
                                    ['/template/edit-smarty', 'id' => $data->id]) . $default.$viewBtn;
                        }
                    ],
                    [
                        'attribute' => 'template_blade',
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data Template */
                            $default = $data->default_template == 3 ?
                                '<i style="color: green" class="btn glyphicon glyphicon-ok"></i>' :
                                Html::a('<i style="color: red" class="btn glyphicon glyphicon-remove"></i>',
                                    ['/template/set-default', 'id' => $data->id, 'default_template' => 3]);
                            return Html::a('<span class="btn btn-primary">Редактировать</span>',
                                    ['/template/edit-blade', 'id' => $data->id]) . $default;
                        }
                    ],

                ],
            ]) ?>
        </div>
    </div>
</div>


