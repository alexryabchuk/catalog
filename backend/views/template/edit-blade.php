<?php

/* @var $this yii\web\View
 * @var $model Template
 */

use common\models\entity\Template;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Список шаблонов';
?>
<div class="user-index">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin() ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?= $form->field($model, 'template_blade')->textarea(['rows' => 25]); ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<?php
$this->registerJsFile("/js/edit_area/edit_area_full.js");
$script = <<<JS
editAreaLoader.init({
	id : "template-template_blade",
	syntax: "php",	
	start_highlight: true,
	language: "ru"		
});
JS;
$this->registerJs($script);
?>

