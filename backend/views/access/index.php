<?php

/* @var $this yii\web\View */

/* @var $searchModel SponsorsPaymentsMethodSearch
 * @var $dataProvider ActiveDataProvider
 * @var $sponsor_id integer
 * @var $per_page integer
 * @var $filter array
 */


use backend\models\search\SponsorsPaymentsMethodSearch;
use common\models\entity\SponsorAccess;
use common\models\entity\Sponsors;
use common\models\enums\AccessStatus;
use common\widgets\ColumnSetting;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Доступы';

CrudAsset::register($this);
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i>
            </button>
            <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                        class="fa fa-filter "></i></button>
            <button type="button" class="btn btn-box-tool" data-toggle="modal"
                    data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
        <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
        <div class="row" id="filter-panel">

            <div class="col-md-2">
                <?= $form->field($searchModel, 'sponsor_id')
                    ->dropDownList(ArrayHelper::merge([null => 'Все'], Sponsors::getSponsorList())); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'login')->textInput(); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'password')->textInput(); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'actual_date')->textInput(['type' => 'date']); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'status')->dropDownList(ArrayHelper::merge([null => 'Все'],
                    AccessStatus::listData())); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'comment')->textInput(); ?>
            </div>
            <div class="col-md-2 ">
                <p style="margin-bottom: 5px;">&nbsp</p>
            </div>
            <div class="col-md-2 col-md-offset-8">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
        <div id="ajaxCrudDatatable">
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->is_deleted) {
                        return ['style' => 'color:red'];
                    }
                    return [];
                },
                'pjax' => true,
                'columns' => [
                    [
                        'attribute' => 'sponsor_id',
                        'visible' => $filter['sponsor_id'] == 'on',
                        'format' => 'raw',
                        'filter' => false,
                        'value' => function ($data) {
                            /* @var $data \backend\models\search\SponsorsAccessSearch */
                            return Html::a($data->sponsor->name, ['/sponsors/update', 'id' => $data->sponsor_id]);
                        }
                    ],
                    [
                        'attribute' => 'sponsor_site_id',
                        'visible' => $filter['sponsor_site_id'] == 'on',
                        'filter' => false,
                        'value' => function ($data) {
                            /* @var $data SponsorAccess */
                            return $data->sponsorSite->name;
                        }
                    ],
                    [
                        'attribute' => 'login',
                        'visible' => $filter['login'] == 'on',
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'password',
                        'visible' => $filter['password'] == 'on',
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'actual_date',
                        'visible' => $filter['password'] == 'on',
                        'filter' => false,
                        'format' => ['date', 'php:d.m.Y']
                    ],
                    [
                        'attribute' => 'status',
                        'visible' => $filter['status'] == 'on',
                        'filter' => false,
                        'value' => function ($data) {
                            return AccessStatus::getLabel($data->status);
                        }
                    ],
                    [
                        'attribute' => 'comment',
                        'visible' => $filter['comment'] == 'on',
                        'filter' => false,
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panelFooterTemplate' => '<div class="kv-panel-pager" style="float: left">{pager}</div>{footer}<div class="clearfix"></div>',
                'panel' => [
                    'heading' => '',
                    'before' => false,
                    'after' => '',
                    'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                        Html::a(10, Url::current(['per-page' => 10]),
                            ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(20, Url::current(['per-page' => 20]),
                            ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                        Html::a(50, Url::current(['per-page' => 50]),
                            ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                        '</div></div>',
                ],
            ]) ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'sponsor_access', 'model' => $searchModel]); ?>
