<?php

/* @var $this yii\web\View */

/* @var $searchModel SponsorsPaymentsMethodSearch
 * @var $dataProvider ActiveDataProvider
 * @var $sponsor_id integer
 * @var $per_page integer
 * @var $filter array
 */


use backend\models\search\SponsorsPaymentsMethodSearch;
use common\models\entity\Sponsors;
use common\models\enums\BacklinkStatus;
use common\widgets\ColumnSetting;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Backlinks';

CrudAsset::register($this);
?>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                    <button id="toogle-filter-btn" type="button" class="btn btn-box-tool"><i
                                class="fa fa-filter "></i></button>
                    <button type="button" class="btn btn-box-tool" data-toggle="modal"
                            data-target="#column-setting-modal"><i class="fa fa-wrench"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['']]) ?>
                <input type="hidden" id="usersearch-per-page" name="per-page" value="<?= $per_page ?>">
                <div class="row" id="filter-panel">
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'sponsor_id')
                            ->dropDownList(ArrayHelper::merge([null => 'Все'], Sponsors::getSponsorList())); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'resource_page')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'period_scan')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'backlink_status')
                            ->dropDownList(ArrayHelper::merge([null => 'Все'], BacklinkStatus::listData())); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'url')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'link_status')->dropDownList([
                            null => 'Все',
                            0 => 'Не активна',
                            1 => 'Активна'
                        ]); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'first_detection')->textInput(['type' => 'date']); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'last_detection')->textInput(['type' => 'date']); ?>
                    </div>
                    <div class="col-md-2 ">
                        <p style="margin-bottom: 5px;">&nbsp</p>
                    </div>
                    <div class="col-md-2 col-md-offset-6">
                        <?= Html::submitButton('Найти', ['class' => 'btn btn-warning btn-block']); ?>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
                <div id="ajaxCrudDatatable">
                    <?= GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'rowOptions' => function ($model, $key, $index, $grid) {
                            if ($model->is_deleted) {
                                return ['style' => 'color:red'];
                            }
                            return [];
                        },
                        'pjax' => true,

                        'columns' => [
                            [
                                'attribute' => 'sponsor_id',
                                'visible' => $filter['sponsor_id'] == 'on',
                                'format' => 'raw',
                                'filter' => false,
                                'value' => function ($data) {
                                    /* @var $data \backend\models\search\SponsorsBacklinksSearch */
                                    return Html::a($data->sponsor->name, ['/sponsors/update', 'id' => $data->sponsor_id]);
                                }
                            ],
                            [
                                'attribute' => 'resource_page',
                                'visible' => $filter['resource_page'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'period_scan',
                                'visible' => $filter['period_scan'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'backlink_status',
                                'visible' => $filter['backlink_status'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    return BacklinkStatus::getLabel($data->backlink_status);
                                }
                            ],
                            [
                                'attribute' => 'url',
                                'visible' => $filter['url'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'link_status',
                                'visible' => $filter['link_status'] == 'on',
                                'filter' => false,
                                'value' => function ($data) {
                                    return $data->link_status ? 'Активна' : 'Не активна';
                                }
                            ],
                            [
                                'attribute' => 'first_detection',
                                'visible' => $filter['first_detection'] == 'on',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'last_detection',
                                'visible' => $filter['last_detection'] == 'on',
                                'filter' => false,
                            ],


                        ],

                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panelFooterTemplate' => '<div class="kv-panel-pager" style="float: left">{pager}</div>{footer}<div class="clearfix"></div>',
                        'panel' => [
                            'heading' => '',
                            'before' => false,
                            'after' => '',
                            'footer' => '<div class="text-right" style="width: 100%"><div style="margin-top:5px">' . 'Отображать по ' .
                                Html::a(10, Url::current(['per-page' => 10]),
                                    ['class' => ($per_page == 10) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(20, Url::current(['per-page' => 20]),
                                    ['class' => ($per_page == 20) ? 'btn btn-primary' : 'btn btn-default']) .
                                Html::a(50, Url::current(['per-page' => 50]),
                                    ['class' => ($per_page == 50) ? 'btn btn-primary' : 'btn btn-default']) .
                                '</div></div>',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?= ColumnSetting::widget(['filter' => $filter, 'tableName' => 'sponsor_backlinks', 'model' => $searchModel]); ?>
