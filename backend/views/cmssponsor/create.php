<?php

/* @var $this yii\web\View */
/* @var $model User */

use common\models\User;

$this->title = 'Создать CMS спонсора';
?>
<div class="payment-system-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
