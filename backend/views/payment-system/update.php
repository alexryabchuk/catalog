<?php

/* @var $this yii\web\View */

/* @var $model User */

use common\models\User;

$this->title = 'Редактировать платежную систему';
?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
