<?php

namespace backend\models\filters;

use yii\base\Model;

class ReviewForm extends Model
{
    public $review;

    public function attributeLabels()
    {
        return [
            'review' => '$review',
        ];
    }
}
