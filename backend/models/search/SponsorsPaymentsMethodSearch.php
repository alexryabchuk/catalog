<?php

namespace backend\models\search;

use common\models\entity\SponsorPaymentsMethod;
use yii\data\ActiveDataProvider;

class SponsorsPaymentsMethodSearch extends SponsorPaymentsMethod
{

    public function rules()
    {
        return [
            [
                [
                    'payment_system_id',
                    'min_pay',
                    'fee',
                    'hold',
                    'status',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorPaymentsMethod::find();
        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                SponsorPaymentsMethod::tableName() . '.payment_system_id' => $this->payment_system_id,
                SponsorPaymentsMethod::tableName() . '.status' => $this->status,

            ]);

        $query
            ->andFilterWhere(['like', SponsorPaymentsMethod::tableName() . '.min_pay', $this->min_pay])
            ->andFilterWhere(['like', SponsorPaymentsMethod::tableName() . '.fee', $this->fee])
            ->andFilterWhere(['like', SponsorPaymentsMethod::tableName() . '.hold', $this->hold]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'payment_system_id' => [
                'asc' => [SponsorPaymentsMethod::tableName() . '.name' => SORT_ASC],
                'desc' => [SponsorPaymentsMethod::tableName() . '.name' => SORT_DESC],
            ],
            'email' => [
                'asc' => [SponsorPaymentsMethod::tableName() . '.slug' => SORT_ASC],
                'desc' => [SponsorPaymentsMethod::tableName() . '.slug' => SORT_DESC],
            ],
            'status' => [
                'asc' => [SponsorPaymentsMethod::tableName() . '.description' => SORT_ASC],
                'desc' => [SponsorPaymentsMethod::tableName() . '.description' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}