<?php

namespace backend\models\search;

use common\models\entity\SponsorPaymentsHistory;
use yii\data\ActiveDataProvider;

class SponsorsPaymentsHistorySearch extends SponsorPaymentsHistory
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'payments_method_id',
                    'date_pay',
                    'sum_pay',
                    'is_deleted',
                    'status',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorPaymentsHistory::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $this->date = $this->date_pay != null ? date('Y-m-d',
            strtotime($this->date_pay)) : $this->date;
        $query
            ->andFilterWhere([
                SponsorPaymentsHistory::tableName() . '.payment_method_id' => $this->payments_method_id,
                SponsorPaymentsHistory::tableName() . '.date_pay' => $this->date,
                SponsorPaymentsHistory::tableName() . '.status' => $this->status,
                SponsorPaymentsHistory::tableName() . '.sponsor_id' => $this->sponsor_id,

            ]);

        $query
            ->andFilterWhere(['like', SponsorPaymentsHistory::tableName() . '.sum_pay', $this->sum_pay]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'payments_method_id' => [
                'asc' => [SponsorPaymentsHistory::tableName() . '.payments_method_id' => SORT_ASC],
                'desc' => [SponsorPaymentsHistory::tableName() . '.payments_method_id' => SORT_DESC],
            ],
            'date_pay' => [
                'asc' => [SponsorPaymentsHistory::tableName() . '.date_pay' => SORT_ASC],
                'desc' => [SponsorPaymentsHistory::tableName() . '.date_pay' => SORT_DESC],
            ],
            'sum_pay' => [
                'asc' => [SponsorPaymentsHistory::tableName() . '.sum_pay' => SORT_ASC],
                'desc' => [SponsorPaymentsHistory::tableName() . '.sum_pay' => SORT_DESC],
            ],
            'status' => [
                'asc' => [SponsorPaymentsHistory::tableName() . '.description' => SORT_ASC],
                'desc' => [SponsorPaymentsHistory::tableName() . '.description' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}