<?php

namespace backend\models\search;

use common\models\entity\SponsorSupport;
use yii\data\ActiveDataProvider;

class SponsorsSupportSearch extends SponsorSupport
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'support_id',
                    'contact',
                    'response_time',
                    'language',
                    'comment'
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorSupport::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['support_id' => $this->support_id]);

        $query
            ->andFilterWhere(['like', SponsorSupport::tableName() . '.contact', $this->contact])
        ->andFilterWhere(['like', SponsorSupport::tableName() . '.response_time', $this->response_time])
        ->andFilterWhere(['like', SponsorSupport::tableName() . '.language', $this->language])
        ->andFilterWhere(['like', SponsorSupport::tableName() . '.comment', $this->comment]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'support_id' => [
                'asc' => [SponsorSupport::tableName() . '.support_id' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.support_id' => SORT_DESC],
            ],
            'contact' => [
                'asc' => [SponsorSupport::tableName() . '.contact' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.contact' => SORT_DESC],
            ],
            'response_time' => [
                'asc' => [SponsorSupport::tableName() . '.response_time' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.response_time' => SORT_DESC],
            ],
            'language' => [
                'asc' => [SponsorSupport::tableName() . '.language' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.language' => SORT_DESC],
            ],
            'comment' => [
                'asc' => [SponsorSupport::tableName() . '.comment' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.comment' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}