<?php

namespace backend\models\search;

use common\models\entity\SponsorPaymentsSales;
use yii\data\ActiveDataProvider;

class SponsorsPaymentsSalesSearch extends SponsorPaymentsSales
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'period_month',
                    'period_years',
                    'unq',
                    'sales',
                    'sales_usd',
                    'rebills',
                    'rebills_usd',
                    'is_deleted',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorPaymentsSales::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                SponsorPaymentsSales::tableName() . '.period_month' => $this->period_month,
                SponsorPaymentsSales::tableName() . '.period_years' => $this->period_years,
                SponsorPaymentsSales::tableName() . '.sponsor_id' => $this->sponsor_id
            ]);

        $query
            ->andFilterWhere(['like', SponsorPaymentsSales::tableName() . '.unq', $this->unq])
            ->andFilterWhere(['like', SponsorPaymentsSales::tableName() . '.sales', $this->sales])
            ->andFilterWhere(['like', SponsorPaymentsSales::tableName() . '.sales_usd', $this->sales_usd])
            ->andFilterWhere(['like', SponsorPaymentsSales::tableName() . '.rebills', $this->rebills])
            ->andFilterWhere(['like', SponsorPaymentsSales::tableName() . '.rebills_usd', $this->rebills_usd]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'typeriod_monthpe' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.period_month' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.period_month' => SORT_DESC],
            ],
            'period_years' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.period_years' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.period_years' => SORT_DESC],
            ],
            'unq' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.unq' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.unq' => SORT_DESC],
            ],
            'sales' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.sales' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.sales' => SORT_DESC],
            ],
            'sales_usd' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.sales_usd' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.sales_usd' => SORT_DESC],
            ],
            'rebills' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.rebills' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.rebills' => SORT_DESC],
            ],
            'rebills_usd' => [
                'asc' => [SponsorPaymentsSales::tableName() . '.rebills_usd' => SORT_ASC],
                'desc' => [SponsorPaymentsSales::tableName() . '.rebills_usd' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}