<?php

namespace backend\models\search;

use common\models\entity\SponsorsReview;
use common\models\entity\SponsorSupport;
use yii\data\ActiveDataProvider;

class SponsorsReviewSearch extends SponsorsReview
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'param',
                    'value',
                    'score',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorsReview::find();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['score' => $this->score]);

        $query
            ->andFilterWhere(['like', SponsorsReview::tableName() . '.param', $this->param])
            ->andFilterWhere(['like', SponsorsReview::tableName() . '.value', $this->value]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'score' => [
                'asc' => [SponsorSupport::tableName() . '.score' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.score' => SORT_DESC],
            ],
            'param' => [
                'asc' => [SponsorSupport::tableName() . '.param' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.param' => SORT_DESC],
            ],
            'value' => [
                'asc' => [SponsorSupport::tableName() . '.value' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.value' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}