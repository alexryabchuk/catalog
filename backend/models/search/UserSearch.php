<?php

namespace backend\models\search;

use common\models\User;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{

    public function rules()
    {
        return [
            [['username', 'email', 'status', 'role'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'role' => $this->role,
            'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', User::tableName() . '.username', $this->username])
            ->andFilterWhere(['like', User::tableName() . '.email', $this->email]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'username' => [
                'asc' => [User::tableName() . '.username' => SORT_ASC],
                'desc' => [User::tableName() . '.username' => SORT_DESC],
            ],
            'email' => [
                'asc' => [User::tableName() . '.email' => SORT_ASC],
                'desc' => [User::tableName() . '.email' => SORT_DESC],
            ],
            'status' => [
                'asc' => [User::tableName() . '.status' => SORT_ASC],
                'desc' => [User::tableName() . '.status' => SORT_DESC],
            ],
            'role' => [
                'asc' => [User::tableName() . '.role' => SORT_ASC],
                'desc' => [User::tableName() . '.role' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}