<?php

namespace backend\models\search;

use common\models\entity\Sponsors;
use yii\data\ActiveDataProvider;

class SponsorsSearch extends Sponsors
{

    public function rules()
    {
        return [
            [
                [
                    'name',
                    'slug',
                    'ref_url',
                    'domain',
                    'description',
                    'publication_status',
                    'recommendation_status',
                    'not_recommendation_reason',
                    'general_company_id',
                    'cms_id',
                    'image'
                ],
                'safe'
            ]
        ];
    }

    public function search($params)
    {
        $query = Sponsors::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                Sponsors::tableName() .'.publication_status' => $this->publication_status,
                Sponsors::tableName() .'.recommendation_status' => $this->recommendation_status,
                Sponsors::tableName() .'.general_company_id' => $this->general_company_id,
                Sponsors::tableName() .'.cms_id' => $this->cms_id,

            ]);

        $query
            ->andFilterWhere(['like', Sponsors::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', Sponsors::tableName() . '.slug', $this->slug])
            ->andFilterWhere(['like', Sponsors::tableName() . '.ref_url', $this->ref_url])
            ->andFilterWhere(['like', Sponsors::tableName() . '.domain', $this->domain])
            ->andFilterWhere(['like', Sponsors::tableName() . '.description', $this->description]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'username' => [
                'asc' => [Sponsors::tableName() . '.name' => SORT_ASC],
                'desc' => [Sponsors::tableName() . '.name' => SORT_DESC],
            ],
            'email' => [
                'asc' => [Sponsors::tableName() . '.slug' => SORT_ASC],
                'desc' => [Sponsors::tableName() . '.slug' => SORT_DESC],
            ],
            'status' => [
                'asc' => [Sponsors::tableName() . '.description' => SORT_ASC],
                'desc' => [Sponsors::tableName() . '.description' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}