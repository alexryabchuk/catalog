<?php

namespace backend\models\search;

use common\models\entity\SponsorBacklinks;
use yii\data\ActiveDataProvider;

class SponsorsBacklinksSearch extends SponsorBacklinks
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'resource_page',
                    'period_scan',
                    'backlink_status',
                    'url',
                    'link_status',
                    'first_detection',
                    'last_detection',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorBacklinks::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $first_detection = $this->first_detection ? $this->first_detection : null;
        $last_detection = $this->last_detection ? $this->last_detection : null;
        $query
            ->andFilterWhere([SponsorBacklinks::tableName() . '.first_detection' => $first_detection])
            ->andFilterWhere([SponsorBacklinks::tableName() . '.last_detection' => $last_detection])
            ->andFilterWhere([SponsorBacklinks::tableName() . '.backlink_status' => $this->backlink_status])
            ->andFilterWhere([SponsorBacklinks::tableName() . '.sponsor_id' => $this->sponsor_id])
            ->andFilterWhere([SponsorBacklinks::tableName() . '.link_status' => $this->link_status]);
        $query
            ->andFilterWhere(['like', SponsorBacklinks::tableName() . '.resource_page', $this->resource_page])
            ->andFilterWhere(['like', SponsorBacklinks::tableName() . '.period_scan', $this->period_scan])
            ->andFilterWhere(['like', SponsorBacklinks::tableName() . '.url', $this->url]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'first_detection' => [
                'asc' => [SponsorBacklinks::tableName() . '.first_detection' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.first_detection' => SORT_DESC],
            ],
            'last_detection' => [
                'asc' => [SponsorBacklinks::tableName() . '.last_detection' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.last_detection' => SORT_DESC],
            ],
            'backlink_status' => [
                'asc' => [SponsorBacklinks::tableName() . '.backlink_status' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.backlink_status' => SORT_DESC],
            ],
            'link_status' => [
                'asc' => [SponsorBacklinks::tableName() . '.link_status' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.link_status' => SORT_DESC],
            ],
            'resource_page' => [
                'asc' => [SponsorBacklinks::tableName() . '.resource_page' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.resource_page' => SORT_DESC],
            ],
            'period_scan' => [
                'asc' => [SponsorBacklinks::tableName() . '.period_scan' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.period_scan' => SORT_DESC],
            ],
            'url' => [
                'asc' => [SponsorBacklinks::tableName() . '.url' => SORT_ASC],
                'desc' => [SponsorBacklinks::tableName() . '.url' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}