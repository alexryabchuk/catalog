<?php

namespace backend\models\search;

use common\models\entity\SponsorAccess;
use yii\data\ActiveDataProvider;

class SponsorsAccessSearch extends SponsorAccess
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'sponsor_id',
                    'sponsor_site_id',
                    'login',
                    'password',
                    'actual_date',
                    'status',
                    'comment',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorAccess::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $date = $this->actual_date ? date('Y-m-d', strtotime($this->actual_date)) : null;
        $query
            ->andFilterWhere([SponsorAccess::tableName() . '.actual_date' => $date])
            ->andFilterWhere([SponsorAccess::tableName() . '.sponsor_site_id' => $this->sponsor_site_id])
            ->andFilterWhere([SponsorAccess::tableName() . '.sponsor_id' => $this->sponsor_id])
            ->andFilterWhere([SponsorAccess::tableName() . '.status' => $this->status]);

        $query
            ->andFilterWhere(['like', SponsorAccess::tableName() . '.login', $this->login])
            ->andFilterWhere(['like', SponsorAccess::tableName() . '.password', $this->password])
            ->andFilterWhere(['like', SponsorAccess::tableName() . '.comment', $this->comment]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'actual_date' => [
                'asc' => [SponsorAccess::tableName() . '.actual_date' => SORT_ASC],
                'desc' => [SponsorAccess::tableName() . '.actual_date' => SORT_DESC],
            ],
            'sponsor_site_id' => [
                'asc' => [SponsorAccess::tableName() . '.sponsor_site_id' => SORT_ASC],
                'desc' => [SponsorAccess::tableName() . '.sponsor_site_id' => SORT_DESC],
            ],
            'status' => [
                'asc' => [SponsorAccess::tableName() . '.status' => SORT_ASC],
                'desc' => [SponsorAccess::tableName() . '.status' => SORT_DESC],
            ],
            'login' => [
                'asc' => [SponsorAccess::tableName() . '.login' => SORT_ASC],
                'desc' => [SponsorAccess::tableName() . '.login' => SORT_DESC],
            ],
            'password' => [
                'asc' => [SponsorAccess::tableName() . '.password' => SORT_ASC],
                'desc' => [SponsorAccess::tableName() . '.password' => SORT_DESC],
            ],
            'comment' => [
                'asc' => [SponsorAccess::tableName() . '.comment' => SORT_ASC],
                'desc' => [SponsorAccess::tableName() . '.comment' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}