<?php

namespace backend\models\search;

use common\models\entity\SponsorSites;
use common\models\entity\SponsorSupport;
use yii\data\ActiveDataProvider;

class SponsorsSitesSearch extends SponsorSites
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'name',
                    'pps_status',
                    'main_category',
                    'category1',
                    'category2',
                    'category3',
                    'category4',
                    'category5',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorSites::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                SponsorSites::tableName() . '.main_category' => $this->main_category
            ]);

        $query
            ->andFilterWhere(['like', SponsorSites::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', SponsorSites::tableName() . '.pps_status', $this->pps_status]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'name' => [
                'asc' => [SponsorSupport::tableName() . '.name' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.name' => SORT_DESC],
            ],
            'pps_status' => [
                'asc' => [SponsorSupport::tableName() . '.pps_status' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.pps_status' => SORT_DESC],
            ],
            'category' => [
                'asc' => [SponsorSupport::tableName() . '.category' => SORT_ASC],
                'desc' => [SponsorSupport::tableName() . '.category' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}