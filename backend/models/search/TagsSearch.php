<?php

namespace backend\models\search;

use common\models\entity\CategorySites;
use common\models\entity\Tags;
use yii\data\ActiveDataProvider;

class TagsSearch extends Tags
{

	public function rules()
	{
		return [
			[['parent_id', 'name', 'slug', 'description', 'image', 'is_visible'], 'safe']
		];
	}

	public function search($params)
	{
		$query = Tags::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->addSort($dataProvider);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}
		$query
			->andFilterWhere([Tags::tableName() . '.is_visible' => $this->is_visible])
			->andFilterWhere([Tags::tableName() . '.parent_id' => $this->parent_id]);


		$query
			->andFilterWhere(['like', Tags::tableName() . '.name', $this->name])
			->andFilterWhere(['like', Tags::tableName() . '.slug', $this->slug])
			->andFilterWhere(['like', Tags::tableName() . '.description', $this->description]);

		return $dataProvider;
	}

	/**
	 * Добавляет сортировку к полям с доп. обработкой
	 *
	 * @param ActiveDataProvider $dataProvider
	 *
	 * @return void
	 */
	private function addSort(ActiveDataProvider $dataProvider)
	{
		$sorts = [
			'name' => [
				'asc' => [Tags::tableName() . '.name' => SORT_ASC],
				'desc' => [Tags::tableName() . '.name' => SORT_DESC],
			],
			'slug' => [
				'asc' => [Tags::tableName() . '.slug' => SORT_ASC],
				'desc' => [Tags::tableName() . '.slug' => SORT_DESC],
			],
			'description' => [
				'asc' => [Tags::tableName() . '.description' => SORT_ASC],
				'desc' => [Tags::tableName() . '.description' => SORT_DESC],
			],
		];
		$dataProvider->sort->attributes += $sorts;
	}
}