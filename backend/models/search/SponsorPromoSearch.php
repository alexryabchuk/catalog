<?php

namespace backend\models\search;

use common\models\entity\SponsorPromo;
use yii\data\ActiveDataProvider;

class SponsorPromoSearch extends SponsorPromo
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'name',
                    'content',
                    'start_date',
                    'end_date',
                    'url',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorPromo::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $start_date = $this->start_date ? date('Y-m-d', strtotime($this->start_date)) : null;
        $end_date = $this->end_date ? date('Y-m-d', strtotime($this->end_date)) : null;

        $query
            ->andFilterWhere([SponsorPromo::tableName() . '.start_date' => $start_date])
            ->andFilterWhere([SponsorPromo::tableName() . '.sponsor_id' => $this->sponsor_id])
            ->andFilterWhere([SponsorPromo::tableName() . '.end_date' => $end_date]);

        $query
            ->andFilterWhere(['like', SponsorPromo::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', SponsorPromo::tableName() . '.content', $this->content])
            ->andFilterWhere(['like', SponsorPromo::tableName() . '.url', $this->url]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'start_date' => [
                'asc' => [SponsorPromo::tableName() . '.start_date' => SORT_ASC],
                'desc' => [SponsorPromo::tableName() . '.start_date' => SORT_DESC],
            ],
            'end_date' => [
                'asc' => [SponsorPromo::tableName() . '.end_date' => SORT_ASC],
                'desc' => [SponsorPromo::tableName() . '.end_date' => SORT_DESC],
            ],
            'name' => [
                'asc' => [SponsorPromo::tableName() . '.name' => SORT_ASC],
                'desc' => [SponsorPromo::tableName() . '.name' => SORT_DESC],
            ],
            'content' => [
                'asc' => [SponsorPromo::tableName() . '.content' => SORT_ASC],
                'desc' => [SponsorPromo::tableName() . '.content' => SORT_DESC],
            ],
            'url' => [
                'asc' => [SponsorPromo::tableName() . '.url' => SORT_ASC],
                'desc' => [SponsorPromo::tableName() . '.url' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}