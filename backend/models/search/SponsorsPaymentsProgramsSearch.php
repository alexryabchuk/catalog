<?php

namespace backend\models\search;

use common\models\entity\SponsorPaymentsProgram;
use yii\data\ActiveDataProvider;

class SponsorsPaymentsProgramsSearch extends SponsorPaymentsProgram
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'type',
                    'sum_percent',
                    'comment',
                    'is_deleted',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorPaymentsProgram::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                SponsorPaymentsProgram::tableName() . '.type' => $this->type,

            ]);

        $query
            ->andFilterWhere(['like', SponsorPaymentsProgram::tableName() . '.sum_percent', $this->sum_percent])
            ->andFilterWhere(['like', SponsorPaymentsProgram::tableName() . '.comment', $this->comment]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'type' => [
                'asc' => [SponsorPaymentsProgram::tableName() . '.type' => SORT_ASC],
                'desc' => [SponsorPaymentsProgram::tableName() . '.type' => SORT_DESC],
            ],
            'sum_percent' => [
                'asc' => [SponsorPaymentsProgram::tableName() . '.sum_percent' => SORT_ASC],
                'desc' => [SponsorPaymentsProgram::tableName() . '.sum_percent' => SORT_DESC],
            ],
            'comment' => [
                'asc' => [SponsorPaymentsProgram::tableName() . '.comment' => SORT_ASC],
                'desc' => [SponsorPaymentsProgram::tableName() . '.comment' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}