<?php

namespace backend\models\search;

use common\models\entity\CategorySites;
use yii\data\ActiveDataProvider;

class CategorySitesSearch extends CategorySites
{

	public function rules()
	{
		return [
			[['name', 'slug', 'description', 'image', 'is_active'], 'safe']
		];
	}

	public function search($params)
	{
		$query = CategorySites::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->addSort($dataProvider);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}
		$query
			->andFilterWhere([CategorySites::tableName() . '.is_active' => $this->is_active]);


		$query
			->andFilterWhere(['like', CategorySites::tableName() . '.name', $this->name])
			->andFilterWhere(['like', CategorySites::tableName() . '.slug', $this->slug])
			->andFilterWhere(['like', CategorySites::tableName() . '.description', $this->description]);

		return $dataProvider;
	}

	/**
	 * Добавляет сортировку к полям с доп. обработкой
	 *
	 * @param ActiveDataProvider $dataProvider
	 *
	 * @return void
	 */
	private function addSort(ActiveDataProvider $dataProvider)
	{
		$sorts = [
			'username' => [
				'asc' => [CategorySites::tableName() . '.name' => SORT_ASC],
				'desc' => [CategorySites::tableName() . '.name' => SORT_DESC],
			],
			'email' => [
				'asc' => [CategorySites::tableName() . '.slug' => SORT_ASC],
				'desc' => [CategorySites::tableName() . '.slug' => SORT_DESC],
			],
			'status' => [
				'asc' => [CategorySites::tableName() . '.description' => SORT_ASC],
				'desc' => [CategorySites::tableName() . '.description' => SORT_DESC],
			],
		];
		$dataProvider->sort->attributes += $sorts;
	}
}