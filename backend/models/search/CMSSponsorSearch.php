<?php

namespace backend\models\search;

use common\models\entity\CMSSponsor;
use yii\data\ActiveDataProvider;

class CMSSponsorSearch extends CMSSponsor
{

    public function rules()
    {
        return [
            [['name', 'slug','description','is_deleted'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = CMSSponsor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', CMSSponsor::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', CMSSponsor::tableName() . '.description', $this->description])
            ->andFilterWhere(['like', CMSSponsor::tableName() . '.slug', $this->slug]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'name' => [
                'asc' => [CMSSponsor::tableName() . '.name' => SORT_ASC],
                'desc' => [CMSSponsor::tableName() . '.name' => SORT_DESC],
            ],
            'slug' => [
                'asc' => [CMSSponsor::tableName() . '.slug' => SORT_ASC],
                'desc' => [CMSSponsor::tableName() . '.slug' => SORT_DESC],
            ],
            'description' => [
                'asc' => [CMSSponsor::tableName() . '.description' => SORT_ASC],
                'desc' => [CMSSponsor::tableName() . '.description' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}