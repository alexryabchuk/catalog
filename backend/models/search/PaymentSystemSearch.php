<?php

namespace backend\models\search;

use common\models\entity\PaymentSystems;
use yii\data\ActiveDataProvider;

class PaymentSystemSearch extends PaymentSystems
{

    public function rules()
    {
        return [
            [['name', 'slug', 'description', 'image'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = PaymentSystems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', PaymentSystems::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', PaymentSystems::tableName() . '.slug', $this->slug])
            ->andFilterWhere(['like', PaymentSystems::tableName() . '.description', $this->description]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'username' => [
                'asc' => [PaymentSystems::tableName() . '.name' => SORT_ASC],
                'desc' => [PaymentSystems::tableName() . '.name' => SORT_DESC],
            ],
            'email' => [
                'asc' => [PaymentSystems::tableName() . '.slug' => SORT_ASC],
                'desc' => [PaymentSystems::tableName() . '.slug' => SORT_DESC],
            ],
            'status' => [
                'asc' => [PaymentSystems::tableName() . '.description' => SORT_ASC],
                'desc' => [PaymentSystems::tableName() . '.description' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}