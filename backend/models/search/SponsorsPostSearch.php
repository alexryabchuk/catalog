<?php

namespace backend\models\search;

use common\models\entity\SponsorPost;
use yii\data\ActiveDataProvider;

class SponsorsPostSearch extends SponsorPost
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'post_date',
                    'status',
                    'assign_sponsor_id',
                    'assign_site_id',
                    'post',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorPost::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $date = $this->post_date ? date('Y-m-d',strtotime($this->post_date)) : null;
        $query
            ->andFilterWhere([SponsorPost::tableName() . '.post_date' => $date])
            ->andFilterWhere([SponsorPost::tableName() . '.status' => $this->status])
            ->andFilterWhere([SponsorPost::tableName() . '.sponsor_id' => $this->sponsor_id])
            ->andFilterWhere([SponsorPost::tableName() . '.assign_sponsor_id'=> $this->assign_sponsor_id])
            ->andFilterWhere([SponsorPost::tableName() . '.assign_site_id'=> $this->assign_site_id]);

        $query
            ->andFilterWhere(['like', SponsorPost::tableName() . '.post', $this->post]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'date' => [
                'asc' => [SponsorPost::tableName() . '.date' => SORT_ASC],
                'desc' => [SponsorPost::tableName() . '.date' => SORT_DESC],
            ],
            'status' => [
                'asc' => [SponsorPost::tableName() . '.status' => SORT_ASC],
                'desc' => [SponsorPost::tableName() . '.status' => SORT_DESC],
            ],
            'assign_sponsor_id' => [
                'asc' => [SponsorPost::tableName() . '.assign_sponsor_id' => SORT_ASC],
                'desc' => [SponsorPost::tableName() . '.assign_sponsor_id' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}