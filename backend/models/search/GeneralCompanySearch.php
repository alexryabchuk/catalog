<?php

namespace backend\models\search;

use common\models\entity\GeneralCompany;
use common\models\entity\Support;
use yii\data\ActiveDataProvider;

class GeneralCompanySearch extends Support
{

    public function rules()
    {
        return [
            [['name', 'slug','is_deleted'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = GeneralCompany::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', GeneralCompany::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', GeneralCompany::tableName() . '.slug', $this->slug]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'name' => [
                'asc' => [GeneralCompany::tableName() . '.name' => SORT_ASC],
                'desc' => [GeneralCompany::tableName() . '.name' => SORT_DESC],
            ],
            'slug' => [
                'asc' => [GeneralCompany::tableName() . '.slug' => SORT_ASC],
                'desc' => [GeneralCompany::tableName() . '.slug' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}