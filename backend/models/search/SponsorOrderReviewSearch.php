<?php

namespace backend\models\search;

use common\models\entity\SponsorOrderReview;
use yii\data\ActiveDataProvider;

class SponsorOrderReviewSearch extends SponsorOrderReview
{
    public $date;

    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'status',
                    'order_date',
                    'e_mail',
                    'order_sponsor_id',
                    'order_url',
                    'recip_URL',
                    'editor',
                    'writer',
                    'pay_amount',
                    'pay_status',
                ],
                'safe'
            ]
        ];
    }

    public function search($params, $sponsor_id = null)
    {
        $query = SponsorOrderReview::find()->notDeleted();

        if ($sponsor_id) {
            $query->andWhere(['sponsor_id' => $sponsor_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->addSort($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $date = $this->order_date ? date('Y-m-d', strtotime($this->order_date)) : null;
        $query
            ->andFilterWhere([SponsorOrderReview::tableName() . '.order_date' => $date])
            ->andFilterWhere([SponsorOrderReview::tableName() . '.status' => $this->status])
            ->andFilterWhere([SponsorOrderReview::tableName() . '.pay_amount' => $this->pay_amount])
            ->andFilterWhere([SponsorOrderReview::tableName() . '.sponsor_id' => $this->sponsor_id])
            ->andFilterWhere([SponsorOrderReview::tableName() . '.pay_status' => $this->pay_status]);

        $query
            ->andFilterWhere(['like', SponsorOrderReview::tableName() . '.e_mail', $this->e_mail])
            ->andFilterWhere(['like', SponsorOrderReview::tableName() . '.recip_URL', $this->recip_URL])
            ->andFilterWhere(['like', SponsorOrderReview::tableName() . '.editor', $this->editor])
            ->andFilterWhere(['like', SponsorOrderReview::tableName() . '.writer', $this->writer]);

        return $dataProvider;
    }

    /**
     * Добавляет сортировку к полям с доп. обработкой
     *
     * @param ActiveDataProvider $dataProvider
     * @return void
     */
    private function addSort(ActiveDataProvider $dataProvider)
    {
        $sorts = [
            'order_date' => [
                'asc' => [SponsorOrderReview::tableName() . '.order_date' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.order_date' => SORT_DESC],
            ],
            'status' => [
                'asc' => [SponsorOrderReview::tableName() . '.status' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.status' => SORT_DESC],
            ],
            'pay_amount' => [
                'asc' => [SponsorOrderReview::tableName() . '.pay_amount' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.pay_amount' => SORT_DESC],
            ],
            'pay_status' => [
                'asc' => [SponsorOrderReview::tableName() . '.pay_status' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.pay_status' => SORT_DESC],
            ],
            'e_mail' => [
                'asc' => [SponsorOrderReview::tableName() . '.e_mail' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.e_mail' => SORT_DESC],
            ],
            'recip_URL' => [
                'asc' => [SponsorOrderReview::tableName() . '.recip_URL' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.recip_URL' => SORT_DESC],
            ],
            'editor' => [
                'asc' => [SponsorOrderReview::tableName() . '.editor' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.editor' => SORT_DESC],
            ],
            'writer' => [
                'asc' => [SponsorOrderReview::tableName() . '.writer' => SORT_ASC],
                'desc' => [SponsorOrderReview::tableName() . '.writer' => SORT_DESC],
            ],
        ];
        $dataProvider->sort->attributes += $sorts;
    }
}