<?php

namespace backend\models\filters;

use yii\base\Model;

class UserFilter extends Model
{
    public $username;
    public $email;
    public $status;
    public $role;

    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'email' => 'email',
            'status' => 'Статус',
            'role' => 'Роль',
        ];
    }
}
