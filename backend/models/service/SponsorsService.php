<?php
namespace backend\models\service;

use backend\models\search\SponsorsPostSearch;
use common\models\entity\SponsorPost;
use common\models\entity\Sponsors;

class SponsorsService {

    public function topSponsors($count = 3){
        return Sponsors::find()->limit($count)->orderBy(['id'=>SORT_DESC])->all();
    }

    public function lastSponsors($count = 3){
        return Sponsors::find()->limit($count)->orderBy(['id'=>SORT_DESC])->all();
    }

    public function lastPosts($count = 3){
        return SponsorPost::find()->limit($count)->orderBy(['id'=>SORT_DESC])->all();
    }

    public function getSponsorInfo ($id) {
        return Sponsors::find()->where(['id'=>$id])->one();
    }



}