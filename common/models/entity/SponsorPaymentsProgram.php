<?php

namespace common\models\entity;

use common\models\query\SponsorPaymentsProgramQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sponsor_payments_program".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property int $type Тип программы
 * @property float $sum_percent СУмма/Процент
 * @property string|null $comment Коментарий
 * @property int|null $is_deleted
 *
 * @property Sponsors $sponsor
 */
class SponsorPaymentsProgram extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_payments_program';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'type', 'sum_percent'], 'required'],
            [['sponsor_id', 'type', 'is_deleted'], 'integer'],
            [['sum_percent'], 'number'],
            [['comment'], 'string'],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::className(),
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'type' => 'Тип программы',
            'sum_percent' => 'Сумма/Процент',
            'comment' => 'Коментарий',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPaymentsProgramQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorPaymentsProgramQuery(get_called_class());
    }
}
