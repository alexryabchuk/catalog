<?php

namespace common\models\entity;

use common\models\query\CategorySitesQuery;
use conquer\helpers\Array2Xml;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category_sites".
 *
 * @property int $id
 * @property string|null $name Название
 * @property string|null $slug Slug(Папка)
 * @property string|null $description Описание
 * @property string|null $image Изображение
 * @property int|null $is_active Статус
 * @property int|null $is_deleted
 */
class CategorySites extends ActiveRecord
{

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_sites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['is_active', 'is_deleted'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Slug(Папка)',
            'description' => 'Описание',
            'image' => 'Изображение',
            'is_active' => 'Статус',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CategorySitesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategorySitesQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getCategoryList()
    {
        return ArrayHelper::map(self::find()->notDeleted()->all(), 'id', 'name');
    }
}
