<?php

namespace common\models\entity;

use common\models\query\SponsorSitesQuery;
use common\models\query\SponsorsQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sponsor_sites".
 *
 * @property int $id
 * @property int|null $sponsor_id Спонсор
 * @property string|null $name Имя сайта
 * @property int|null $pps_status PPS статус
 * @property int|null $main_category Основная категория
 * @property int|null $category1 Категория1
 * @property int|null $category2 Категория2
 * @property int|null $category3 Категория3
 * @property int|null $category4 Категория4
 * @property int|null $category5 Категория5
 * @property int|null $is_deleted
 *
 * @property Sponsors $sponsor
 */
class SponsorSites extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_sites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'sponsor_id',
                    'pps_status',
                    'is_deleted',
                    'main_category',
                    'category1',
                    'category2',
                    'category3',
                    'category4',
                    'category5'
                ],
                'integer'
            ],
            [['name',], 'string', 'max' => 255],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::className(),
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'name' => 'Имя сайта',
            'pps_status' => 'PPS статус',
            'main_category' => 'Основная категория',
            'category1' => 'Дополнительная категория1',
            'category2' => 'Дополнительная категория2',
            'category3' => 'Дополнительная категория3',
            'category4' => 'Дополнительная категория4',
            'category5' => 'Дополнительная категория5',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery|SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorSitesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorSitesQuery(get_called_class());
    }

    public static function getSponsorSites($sponsorId = null)
    {
        return $sponsorId === null ? ArrayHelper::map(SponsorSites::find()->notDeleted()->all(), 'id',
            'name') : ArrayHelper::map(SponsorSites::find()->where(['sponsor_id' => $sponsorId])->notDeleted()->all(),
            'id', 'name');
    }

}
