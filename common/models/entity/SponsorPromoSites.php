<?php

namespace common\models\entity;

/**
 * This is the model class for table "sponsor_promo_sites".
 *
 * @property int $id
 * @property int $promo_id
 * @property int $sites_id
 *
 * @property SponsorPromo $promo
 * @property SponsorSites $sites
 */
class SponsorPromoSites extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_promo_sites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_id', 'sites_id'], 'required'],
            [['promo_id', 'sites_id'], 'integer'],
            [['promo_id'], 'exist', 'skipOnError' => true, 'targetClass' => SponsorPromo::className(), 'targetAttribute' => ['promo_id' => 'id']],
            [['sites_id'], 'exist', 'skipOnError' => true, 'targetClass' => SponsorSites::className(), 'targetAttribute' => ['sites_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_id' => 'Promo ID',
            'sites_id' => 'Sites ID',
        ];
    }

    /**
     * Gets query for [[Promo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(SponsorPromo::className(), ['id' => 'promo_id']);
    }

    /**
     * Gets query for [[Sites]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasOne(SponsorSites::className(), ['id' => 'sites_id']);
    }
}
