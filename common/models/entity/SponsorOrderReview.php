<?php

namespace common\models\entity;

use common\models\query\SponsorOrderReviewQuery;
use common\models\query\SponsorsQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sponsor_order_review".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property int $order_date Дата
 * @property int $status Статус
 * @property string $e_mail E-mail
 * @property int|null $order_sponsor_id Спонсор
 * @property string|null $order_url URL
 * @property string $recip_URL Recip URL
 * @property string $editor Редактор
 * @property string $writer Писатель
 * @property float|null $pay_amount Сумма
 * @property int|null $pay_status
 * @property int|null $is_deleted
 *
 * @property Sponsors $orderSponsor
 * @property Sponsors $sponsor
 */
class SponsorOrderReview extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_order_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'e_mail', 'recip_URL', 'editor', 'writer', 'order_date'], 'required'],
            [['sponsor_id', 'status', 'order_sponsor_id', 'pay_status', 'is_deleted'], 'integer'],
            [['pay_amount'], 'number'],
            [['e_mail', 'order_url', 'recip_URL', 'editor', 'writer'], 'string', 'max' => 255],
            [
                ['order_sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['order_sponsor_id' => 'id']
            ],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'order_date' => 'Дата',
            'status' => 'Статус',
            'e_mail' => 'E-mail',
            'order_sponsor_id' => 'Спонсор',
            'order_url' => 'URL',
            'recip_URL' => 'Recip URL',
            'editor' => 'Редактор',
            'writer' => 'Писатель',
            'pay_amount' => 'Сумма',
            'pay_status' => 'Pay Status',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[OrderSponsor]].
     *
     * @return ActiveQuery|SponsorsQuery
     */
    public function getOrderSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'order_sponsor_id']);
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return ActiveQuery|SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorOrderReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorOrderReviewQuery(get_called_class());
    }
}
