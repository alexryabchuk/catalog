<?php

namespace common\models\entity;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sponsors".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $ref_url
 * @property string $ref_url_date_scan
 * @property string $ref_url_status_scan
 * @property string|null $domain
 * @property string|null $description
 * @property int|null $publication_status
 * @property int|null $recommendation_status
 * @property string|null $not_recommendation_reason
 * @property int|null $general_company_id
 * @property int|null $cms_id
 * @property string|null $image
 * @property int $is_deleted
 *
 * @property CMSSponsor $cms
 * @property GeneralCompany $generalCompany
 */
class Sponsors extends ActiveRecord
{

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'ref_url'], 'required'],
            [
                ['description', 'not_recommendation_reason', 'ref_url_date_scan', 'ref_url_status_scan', 'review_text'],
                'string'
            ],
            [['publication_status', 'recommendation_status', 'general_company_id', 'cms_id', 'is_deleted'], 'integer'],
            [
                ['ad_trafic', 'ad_banners', 'ad_fhg', 'ad_hosted', 'ad_downloadable', 'ad_embedded', 'ad_models'],
                'integer'
            ],
            [['name', 'slug', 'ref_url', 'domain', 'image'], 'string', 'max' => 255],
            [
                ['cms_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CMSSponsor::class,
                'targetAttribute' => ['cms_id' => 'id']
            ],
            [
                ['general_company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => GeneralCompany::class,
                'targetAttribute' => ['general_company_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Slug',
            'ref_url' => 'Реферальная ссылка',
            'domain' => 'Domain',
            'description' => 'Описание',
            'publication_status' => 'Статус публикации',
            'recommendation_status' => 'Статус рекомендации',
            'not_recommendation_reason' => 'Причина блокировки',
            'general_company_id' => 'Компания',
            'cms_id' => 'CMS',
            'image' => 'Изображение',
            'ad_trafic' => 'Traffic Campaignxml',
            'ad_banners' => 'Banners',
            'ad_fhg' => 'fhg',
            'ad_hosted' => 'hosted content',
            'ad_downloadable' => 'downloadable content',
            'ad_embedded' => 'embedded content',
            'ad_models' => 'models links',
            'review_text' => 'Обзор'
        ];
    }

    /**
     * Gets query for [[Cms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCms()
    {
        return $this->hasOne(CMSSponsor::class, ['id' => 'cms_id']);
    }

    /**
     * Gets query for [[GeneralCompany]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralCompany()
    {
        return $this->hasOne(GeneralCompany::class, ['id' => 'general_company_id']);
    }

    public function getSites()
    {
        return $this->hasMany(SponsorSites::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorAccesses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorAccesses()
    {
        return $this->hasMany(SponsorAccess::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorBacklinks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorBacklinks()
    {
        return $this->hasMany(SponsorBacklinks::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorOrderReviews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorOrderReviewsPrograms()
    {
        return $this->hasMany(SponsorOrderReview::class, ['order_sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorOrderReviews0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorOrderReviews()
    {
        return $this->hasMany(SponsorOrderReview::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPaymentsHistories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorPaymentsHistories()
    {
        return $this->hasMany(SponsorPaymentsHistory::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPaymentsMethods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorPaymentsMethods()
    {
        return $this->hasMany(SponsorPaymentsMethod::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[PaymentSystems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentSystems()
    {
        return $this->hasMany(PaymentSystems::class, ['id' => 'payment_system_id'])->viaTable('sponsor_payments_method', ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPaymentsPrograms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorPaymentsPrograms()
    {
        return $this->hasMany(SponsorPaymentsProgram::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPaymentsSales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorPaymentsSales()
    {
        return $this->hasMany(SponsorPaymentsSales::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPosts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorAssignPosts()
    {
        return $this->hasMany(SponsorPost::class, ['assign_sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPosts0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorPosts()
    {
        return $this->hasMany(SponsorPost::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorPromos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorPromos()
    {
        return $this->hasMany(SponsorPromo::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorSites]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorSites()
    {
        return $this->hasMany(SponsorSites::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorSupports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorSupports()
    {
        return $this->hasMany(SponsorSupport::class, ['sponsor_id' => 'id']);
    }

    /**
     * Gets query for [[SponsorsReviews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorsReviews()
    {
        return $this->hasMany(SponsorsReview::class, ['sponsor_id' => 'id']);
    }

    /**
     *
     * @return array
     */
    public static function getSponsorList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
