<?php

namespace common\models\entity;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "payment_systems".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property integer $is_deleted
 */
class CMSSponsor extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CMSSponsor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['is_deleted'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Slug',
            'description' => 'Описание',
        ];
    }

    public static function getCMSSponsorList()
    {
        return ArrayHelper::map(self::find()->where(['is_deleted' => 0])->all(), 'id', 'name');
    }
}
