<?php

namespace common\models\entity;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tags".
 *
 * @property int         $id
 * @property int|null    $parent_id
 * @property string|null $slug Слаг
 * @property int|null    $is_visible
 */
class Tags extends ActiveRecord
{

	public $is_group;
	public $imageFile;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'tags';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['parent_id', 'is_visible','is_group'], 'integer'],
			[['name','description'],'string'],
			[['slug'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'parent_id' => 'Родительская группа',
			'is_group' => 'Группа',
			'slug' => 'Слаг',
			'description' => 'Описание',
			'imageFile' => 'Изображение',
			'is_visible' => 'Видимость',
		];
	}

	public static function getMainGroup()
	{
		return ArrayHelper::map(self::find()->where(['parent_id' => 0])->all(), 'id', 'name');
	}
}
