<?php

namespace common\models\entity;

/**
 * This is the model class for table "sponsors_review".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property string $param
 * @property string $value
 * @property float $score
 *
 * @property Sponsors $sponsor
 */
class SponsorsReview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsors_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'param', 'value', 'score'], 'required'],
            [['sponsor_id'], 'integer'],
            [['score'], 'number'],
            [['param', 'value'], 'string', 'max' => 255],
            [['sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sponsors::className(), 'targetAttribute' => ['sponsor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'param' => 'Param',
            'value' => 'Value',
            'score' => 'Score',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\SponsorsReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SponsorsReviewQuery(get_called_class());
    }
}
