<?php

namespace common\models\entity;

use common\models\query\SponsorPaymentsSalesQuery;

/**
 * This is the model class for table "sponsor_payments_sales".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property int $period_month Период(месяц)
 * @property int $period_years Период(год)
 * @property int $unq UNQ
 * @property int $sales Sales
 * @property float $sales_usd Sales USD
 * @property int $rebills Rebills
 * @property float $rebills_usd Rebills USD
 * @property int|null $is_deleted
 *
 * @property Sponsors $sponsor
 */
class SponsorPaymentsSales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_payments_sales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'period_month', 'period_years', 'unq', 'sales', 'sales_usd', 'rebills', 'rebills_usd'], 'required'],
            [['sponsor_id', 'period_month', 'period_years', 'unq', 'sales', 'rebills', 'is_deleted'], 'integer'],
            [['sales_usd', 'rebills_usd'], 'number'],
            [['sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sponsors::className(), 'targetAttribute' => ['sponsor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'period_month' => 'Период(месяц)',
            'period_years' => 'Период(год)',
            'unq' => 'UNQ',
            'sales' => 'Sales',
            'sales_usd' => 'Sales USD',
            'rebills' => 'Rebills',
            'rebills_usd' => 'Rebills USD',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery|\common\models\query\SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPaymentsSalesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorPaymentsSalesQuery(get_called_class());
    }
}
