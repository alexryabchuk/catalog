<?php

namespace common\models\entity;

use common\models\query\SponsorSitesQuery;
use common\models\query\SponsorsQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sponsor_access".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property int $sponsor_site_id Сайт
 * @property string $login
 * @property string $password
 * @property string|null $actual_date
 * @property int $status
 * @property string|null $comment
 * @property int|null $is_deleted
 *
 * @property SponsorSites $sponsorSite
 * @property Sponsors $sponsor
 */
class SponsorAccess extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_access';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'sponsor_site_id', 'login', 'password', 'status'], 'required'],
            [['sponsor_id', 'sponsor_site_id', 'status', 'is_deleted'], 'integer'],
            [['actual_date'], 'safe'],
            [['login', 'password'], 'string', 'max' => 64],
            [['comment'], 'string', 'max' => 255],
            [
                ['sponsor_site_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SponsorSites::class,
                'targetAttribute' => ['sponsor_site_id' => 'id']
            ],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'sponsor_site_id' => 'Сайт',
            'login' => 'Логин',
            'password' => 'Пароль',
            'actual_date' => 'Актуальная дата',
            'status' => 'Статус',
            'comment' => 'Коментарий',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[SponsorSite]].
     *
     * @return ActiveQuery|SponsorSitesQuery
     */
    public function getSponsorSite()
    {
        return $this->hasOne(SponsorSites::class, ['id' => 'sponsor_site_id']);
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return ActiveQuery|SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorsQuery(get_called_class());
    }
}
