<?php

namespace common\models\entity;

/**
 * This is the model class for table "template".
 *
 * @property int $id
 * @property string|null $name Название
 * @property string|null $template_php Шаблон PHP
 * @property string|null $template_smarty Шаблон Smarty
 * @property string|null $template_blade Шаблон Blade
 * @property int $default_template
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template_php', 'template_smarty', 'template_blade'], 'string'],
            [['default_template'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'template_php' => 'Шаблон PHP',
            'template_smarty' => 'Шаблон Smarty',
            'template_blade' => 'Шаблон Blade',
            'default_template' => 'Default Template',
        ];
    }
}
