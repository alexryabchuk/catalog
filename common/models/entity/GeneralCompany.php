<?php

namespace common\models\entity;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "general_company".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $is_deleted
 */
class GeneralCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'general_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['is_deleted'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Slug',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public static function getGeneralCompanyList($canEmpty = false)
    {
        $companyList = ArrayHelper::map(self::find()->where(['is_deleted' => 0])->all(), 'id', 'name');
        return ArrayHelper::merge([null => 'Отсутствует'], $companyList);
    }
}
