<?php

namespace common\models\entity;

use common\models\query\SponsorPaymentsHistoryQuery;
use Yii;

/**
 * This is the model class for table "sponsor_payments_history".
 *
 * @property int $id
 * @property int $sponsor_id
 * @property int $payments_method_id
 * @property string $date_pay
 * @property float $sum_pay
 * @property int|null $status
 * @property int|null $is_deleted
 *
 * @property SponsorPaymentsMethod $paymentsMethod
 * @property Sponsors $sponsor
 */
class SponsorPaymentsHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_payments_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'payments_method_id', 'date_pay', 'sum_pay'], 'required'],
            [['sponsor_id', 'payments_method_id', 'status', 'is_deleted'], 'integer'],
            [['date_pay'], 'safe'],
            [['sum_pay'], 'number'],
            [
                ['payments_method_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SponsorPaymentsMethod::className(),
                'targetAttribute' => ['payments_method_id' => 'id']
            ],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::className(),
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'payments_method_id' => 'Метод платежа',
            'date_pay' => 'Дата',
            'sum_pay' => 'Сумма',
            'status' => 'Статус',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[PaymentsMethod]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getPaymentsMethod()
    {
        return $this->hasOne(SponsorPaymentsMethod::className(), ['id' => 'payments_method_id']);
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPaymentsHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorPaymentsHistoryQuery(get_called_class());
    }
}
