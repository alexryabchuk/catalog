<?php

namespace common\models\entity;

use common\models\query\SponsorBacklinksQuery;
use common\models\query\SponsorsQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sponsor_backlinks".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property string $resource_page Url на сайте
 * @property int $period_scan Частота парсинга
 * @property int $backlink_status
 * @property string $url Найденный Url
 * @property int|null $link_status Статус ссылки
 * @property string|null $first_detection
 * @property string|null $last_detection
 * @property int|null $is_deleted
 *
 * @property Sponsors $sponsor
 */
class SponsorBacklinks extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_backlinks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'period_scan', 'resource_page', 'url'], 'required'],
            [['sponsor_id', 'period_scan', 'backlink_status', 'link_status', 'is_deleted'], 'integer'],
            [['first_detection', 'last_detection'], 'safe'],
            [['resource_page', 'url'], 'string', 'max' => 255],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'resource_page' => 'Url на сайте',
            'period_scan' => 'Частота парсинга',
            'backlink_status' => 'Backlink Статус',
            'url' => 'Найденный Url',
            'link_status' => 'Статус ссылки',
            'first_detection' => 'Дата первого обнаружения',
            'last_detection' => 'Дата последнего обнаружения',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return ActiveQuery|SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorBacklinksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorBacklinksQuery(get_called_class());
    }
}
