<?php

namespace common\models\entity;

use common\models\enums\PaymentsMethodStatus;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sponsor_payments_method".
 *
 * @property int $id
 * @property int $sponsor_id
 * @property int $payment_system_id
 * @property float $min_pay
 * @property float $fee
 * @property int $hold
 * @property int $status
 *
 * @property PaymentSystems $paymentSystem
 * @property Sponsors $sponsor
 */
class SponsorPaymentsMethod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_payments_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'payment_system_id', 'min_pay', 'status'], 'required'],
            [['sponsor_id', 'payment_system_id', 'hold', 'status'], 'integer'],
            [['min_pay', 'fee'], 'number'],
            [
                ['payment_system_id'],
                'unique',
                'targetAttribute' => ['sponsor_id', 'payment_system_id'],
                'message' => 'У спонсора такая платежная система уже существует.'
            ],
            [
                ['payment_system_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PaymentSystems::class,
                'targetAttribute' => ['payment_system_id' => 'id']
            ],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'payment_system_id' => 'Платежная система',
            'min_pay' => 'Миннимальная сумма',
            'fee' => 'Комисия',
            'hold' => 'Задержка',
            'status' => 'Статус',
        ];
    }

    /**
     * Gets query for [[PaymentSystem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentSystem()
    {
        return $this->hasOne(PaymentSystems::className(), ['id' => 'payment_system_id']);
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    public static function getSponsorPaymentsMethodList($sponsor_id)
    {
        return ArrayHelper::map(self::find()->where([
            'in',
            'status',
            [PaymentsMethodStatus::STATUS_ACTIVE, PaymentsMethodStatus::STATUS_AVIABLE]
        ])->all(), 'id', 'paymentSystem.name');
    }
}
