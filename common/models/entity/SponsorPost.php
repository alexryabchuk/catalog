<?php

namespace common\models\entity;

use common\models\query\SponsorPostQuery;
use common\models\query\SponsorSitesQuery;
use common\models\query\SponsorsQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sponsor_post".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property string $post_date Дата
 * @property int|null $status Статус
 * @property int|null $assign_sponsor_id Спонсор
 * @property int|null $assign_site_id Сайт
 * @property string|null $post Текст
 * @property string|null $title Заголовок
 * @property int|null $is_deleted Статус
 *
 * @property SponsorSites $assignSite
 * @property Sponsors $assignSponsor
 * @property Sponsors $sponsor
 */
class SponsorPost extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'post_date','title'], 'required'],
            [['sponsor_id', 'status', 'assign_sponsor_id', 'assign_site_id', 'is_deleted'], 'integer'],
            [['post_date'], 'safe'],
            [['post','title'], 'string'],
            [['assign_site_id'], 'exist', 'skipOnError' => true, 'targetClass' => SponsorSites::class, 'targetAttribute' => ['assign_site_id' => 'id']],
            [['assign_sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sponsors::class, 'targetAttribute' => ['assign_sponsor_id' => 'id']],
            [['sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sponsors::class, 'targetAttribute' => ['sponsor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'post_date' => 'Дата',
            'status' => 'Статус',
            'assign_sponsor_id' => 'Спонсор',
            'assign_site_id' => 'Сайт',
            'post' => 'Текст',
            'title' => 'Заголовок',
            'is_deleted' => 'Статус',
        ];
    }



    /**
     * Gets query for [[AssignSite]].
     *
     * @return ActiveQuery|SponsorSitesQuery
     */
    public function getAssignSite()
    {
        return $this->hasOne(SponsorSites::class, ['id' => 'assign_site_id']);
    }

    /**
     * Gets query for [[AssignSponsor]].
     *
     * @return ActiveQuery|SponsorsQuery
     */
    public function getAssignSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'assign_sponsor_id']);
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return ActiveQuery|SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'sponsor_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorPostQuery(get_called_class());
    }
}
