<?php

namespace common\models\entity;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "support".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property integer is_deleted
 */
class Support extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'support';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['is_deleted'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'slug' => 'Slug',
        ];
    }

    public static function getSupportList()
    {
        return ArrayHelper::map(self::find()->where(['is_deleted' => 0])->all(), 'id', 'name');
    }
}
