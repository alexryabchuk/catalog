<?php

namespace common\models\entity;

use common\models\query\SponsorsQuery;
use common\models\query\SponsorSupportQuery;

/**
 * This is the model class for table "sponsor_support".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property int $support_id Тип связи
 * @property string|null $contact Контакт
 * @property string|null $response_time Время ответа
 * @property string|null $language Язык
 * @property string|null $comment Комментарий
 * @property int $is_deleted
 *
 * @property Sponsors $sponsor
 */
class SponsorSupport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_support';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'support_id'], 'required'],
            [['sponsor_id', 'is_deleted', 'support_id'], 'integer'],
            [['contact', 'response_time', 'language', 'comment'], 'string', 'max' => 255],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'support_id' => 'Тип связи',
            'contact' => 'Контакт',
            'response_time' => 'Время ответа',
            'language' => 'Язык',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return \yii\db\ActiveQuery|SponsorsQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }

    /**
     * Gets query for [[Support]].
     *
     */
    public function getSupport()
    {
        return $this->hasOne(Support::class, ['id' => 'support_id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorSupportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorSupportQuery(get_called_class());
    }
}
