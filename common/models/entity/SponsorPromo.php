<?php

namespace common\models\entity;

use common\models\query\SponsorPromoQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sponsor_promo".
 *
 * @property int $id
 * @property int $sponsor_id Спонсор
 * @property string $name Название
 * @property string|null $image Картинка
 * @property string|null $content Текст
 * @property string $start_date Дата начала
 * @property string|null $end_date Дата конца
 * @property string $url Ссылка
 * @property int|null $is_deleted
 *
 * @property Sponsors $sponsor
 * @property SponsorPromoSites[] $sponsorPromoSites
 */
class SponsorPromo extends ActiveRecord
{
    public $imageFile;
    public $sites;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsor_promo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'name', 'start_date', 'url'], 'required'],
            [['sponsor_id', 'is_deleted'], 'integer'],
            [['content'], 'string'],
            [['start_date', 'end_date', 'sites'], 'safe'],
            [['name', 'image', 'url'], 'string', 'max' => 255],
            [
                ['sponsor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Sponsors::class,
                'targetAttribute' => ['sponsor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponsor_id' => 'Спонсор',
            'name' => 'Название',
            'image' => 'Картинка',
            'content' => 'Текст',
            'start_date' => 'Дата начала',
            'end_date' => 'Дата конца',
            'url' => 'Ссылка',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Sponsor]].
     *
     * @return ActiveQuery|Sponsors
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::class, ['id' => 'sponsor_id']);
    }

    /**
     * Gets query for [[SponsorPromoSites]].
     *
     * @return ActiveQuery|SponsorPromoSites
     */
    public function getSponsorPromoSites()
    {
        return $this->hasMany(SponsorPromoSites::class, ['promo_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPromoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorPromoQuery(get_called_class());
    }

    public function afterFind()
    {
        parent::afterFind();
        /* @var $sites SponsorPromoSites */
        $sites = SponsorPromoSites::find()->where(['promo_id' => $this->id])->all();
        if ($sites) {
            foreach ($sites as $site) {
                $this->sites[] = $site->sites_id;
            }
        }

    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        SponsorPromoSites::deleteAll(['promo_id' => $this->id]);
        if ($this->sites) {
            foreach ($this->sites as $site) {
                $promoSite = new SponsorPromoSites();
                $promoSite->promo_id = $this->id;
                $promoSite->sites_id = $site;
                $promoSite->save();
            }
        }

    }

    public function sitesAsString()
    {
        $st = '';
        if ($this->sites) {
            foreach ($this->sites as $site) {
                $st .= '<p>' . SponsorSites::findOne($site)->name . '</p>';
            }
        }
        return $st;
    }
}
