<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статуси Backlink
 */
class BacklinkStatus extends BaseEnum
{
    const STATUS_NOT = 0;
    const STATUS_RECOURSE = 1;
    const STATUS_PAYSITE = 2;

    public static $list = [
        self::STATUS_NOT => 'Нет',
        self::STATUS_RECOURSE => 'RECOURSE',
        self::STATUS_PAYSITE => 'PAYSITE',
    ];
}