<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статуси методов платежей
 */
class PaymentsMethodStatus extends BaseEnum
{
    const STATUS_DELETED = 3;
    const STATUS_AVIABLE = 2;
    const STATUS_ACTIVE = 1;

    public static $list = [
        self::STATUS_ACTIVE => 'Активен',
        self::STATUS_AVIABLE => 'Возможен',
        self::STATUS_DELETED => 'Удален',
    ];
}