<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статусы закаов обзоров
 */
class OrderReviewStatus extends BaseEnum
{
    const STATUS_NEW = 1;
    const STATUS_ORDER_SPONSOR = 2;
    const STATUS_IN_WORK = 3;
    const STATUS_REVIEW_READY = 4;
    const STATUS_ACCESS_PROBLEM = 5;
    const STATUS_READY = 6;
    const STATUS_CANCEL = 7;
    const STATUS_DELETED = 8;

    public static $list = [
        self::STATUS_NEW => 'Новый заказ',
        self::STATUS_ORDER_SPONSOR => 'Заявка от спонсора',
        self::STATUS_IN_WORK => 'Принят в работу',
        self::STATUS_REVIEW_READY => 'Обзоры готовы',
        self::STATUS_ACCESS_PROBLEM => 'Проблема с доступом',
        self::STATUS_READY => 'Выполнен',
        self::STATUS_CANCEL => 'Отменен',
        self::STATUS_DELETED => 'Удален',
    ];
}