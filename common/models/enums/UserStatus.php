<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статуси пользователей
 */
class UserStatus extends BaseEnum
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    public static $list = [
        self::STATUS_DELETED => 'Удален',
        self::STATUS_INACTIVE => 'Не активен',
        self::STATUS_ACTIVE => 'Активен',
    ];
}