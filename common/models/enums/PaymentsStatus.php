<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статуси методов платежей
 */
class PaymentsStatus extends BaseEnum
{
    const STATUS_APPLY = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_PROBLEM = 3;
    const STATUS_DEBT = 4;

    public static $list = [
        self::STATUS_APPLY => 'Принят',
        self::STATUS_IN_PROGRESS => 'В процессе',
        self::STATUS_PROBLEM => 'Проблема',
        self::STATUS_DEBT => 'Долг',
    ];
}