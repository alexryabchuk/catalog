<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статуси доступа
 */
class AccessStatus extends BaseEnum
{
    const STATUS_ACTIVE = 1;
    const STATUS_ERROR = 2;
    const STATUS_OVERDUE = 3;

    public static $list = [
        self::STATUS_ACTIVE => 'Активен',
        self::STATUS_ERROR => 'Ошибка',
        self::STATUS_OVERDUE => 'Просрочен',
    ];
}