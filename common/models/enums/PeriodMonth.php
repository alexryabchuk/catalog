<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Месяца года
 */
class PeriodMonth extends BaseEnum
{
    const MONTH1 = 1;
    const MONTH2 = 2;
    const MONTH3 = 3;
    const MONTH4 = 4;
    const MONTH5 = 5;
    const MONTH6 = 6;
    const MONTH7 = 7;
    const MONTH8 = 8;
    const MONTH9 = 9;
    const MONTH10 = 10;
    const MONTH11 = 11;
    const MONTH12 = 12;

    public static $list = [
        self::MONTH1 => 'Январь',
        self::MONTH2 => 'Февраль',
        self::MONTH3 => 'Март',
        self::MONTH4 => 'Апрель',
        self::MONTH5 => 'Май',
        self::MONTH6 => 'Июнь',
        self::MONTH7 => 'Июль',
        self::MONTH8 => 'Август',
        self::MONTH9 => 'Сентябрь',
        self::MONTH10 => 'Октябрь',
        self::MONTH11 => 'Ноябрь',
        self::MONTH12 => 'Декабрь',
    ];
}