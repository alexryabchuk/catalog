<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Месяца года
 */
class PeriodYears extends BaseEnum
{
    const YEAR2019 = 2019;
    const YEAR2020 = 2020;
    const YEAR2021 = 2021;
    const YEAR2022 = 2022;
    const YEAR2023 = 2023;
    const YEAR2024 = 2024;
    const YEAR2025 = 2025;
    const YEAR2026 = 2026;
    const YEAR2027 = 2027;
    const YEAR2028 = 2028;

    public static $list = [
        self::YEAR2019 => '2019',
        self::YEAR2020 => '2020',
        self::YEAR2021 => '2021',
        self::YEAR2022 => '2022',
        self::YEAR2023 => '2023',
        self::YEAR2024 => '2024',
        self::YEAR2025 => '2025',
        self::YEAR2026 => '2026',
        self::YEAR2027 => '2027',
        self::YEAR2028 => '2028',
    ];
}