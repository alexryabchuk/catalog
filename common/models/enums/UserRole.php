<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Роли пользователей
 */
class UserRole extends BaseEnum
{
    const ADMIN = 1;
    const EDITOR = 2;
    const WRITER = 3;
    const USER = 9;

    public static $list = [
        self::ADMIN => 'Администратор',
        self::EDITOR => 'Редактор',
        self::WRITER => 'Писатель',
        self::USER => 'Пользователь',
    ];
}