<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 *  Статуси методов платежей
 */
class PaymentsProgramTypes extends BaseEnum
{
    const TYPE_REVSHARE = 1;
    const TYPE_PPS = 2;
    const TYPE_WM_REF = 3;
    const TYPE_OTHER = 99;

    public static $list = [
        self::TYPE_REVSHARE => 'Revshare',
        self::TYPE_PPS => 'PPS',
        self::TYPE_WM_REF => 'WM Ref',
        self::TYPE_OTHER => 'Другой',
    ];
}