<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorPaymentsProgram]].
 *
 * @see \common\models\entity\SponsorPaymentsProgram
 */
class SponsorPaymentsProgramQuery extends \yii\db\ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

}
