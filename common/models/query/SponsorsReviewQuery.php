<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorsReviewSearch]].
 *
 * @see \common\models\entity\SponsorsReview
 */
class SponsorsReviewQuery extends \yii\db\ActiveQuery
{

    /**
     * {@inheritdoc}
     * @return \common\models\entity\SponsorsReview[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\entity\SponsorsReview|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
