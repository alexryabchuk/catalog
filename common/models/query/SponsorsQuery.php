<?php

namespace common\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\entity\Sponsor]].
 *
 * @see \common\models\entity\Sponsor
 */
class SponsorsQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }


}
