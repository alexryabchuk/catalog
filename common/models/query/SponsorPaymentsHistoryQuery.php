<?php

namespace common\models\query;

use common\models\entity\SponsorPaymentsHistory;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[SponsorPaymentsHistory]].
 *
 * @see SponsorPaymentsHistory
 */
class SponsorPaymentsHistoryQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere([SponsorPaymentsHistory::tableName() . '.is_deleted' => false]);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPaymentsHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPaymentsHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
