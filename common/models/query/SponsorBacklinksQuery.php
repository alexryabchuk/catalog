<?php

namespace common\models\query;

use common\models\entity\SponsorBacklinks;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorBacklinks]].
 *
 * @see \common\models\entity\SponsorBacklinks
 */
class SponsorBacklinksQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    /**
     * {@inheritdoc}
     * @return SponsorBacklinks[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SponsorBacklinks|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
