<?php

namespace common\models\query;

use common\models\entity\SponsorPost;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorPost]].
 *
 * @see \common\models\entity\SponsorPost
 */
class SponsorPostQuery extends \yii\db\ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => false]);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPost[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPost|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
