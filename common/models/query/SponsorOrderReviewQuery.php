<?php

namespace common\models\query;

use common\models\entity\SponsorOrderReview;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorOrderReview]].
 *
 * @see \common\models\entity\SponsorOrderReview
 */
class SponsorOrderReviewQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    /**
     * {@inheritdoc}
     * @return SponsorOrderReview[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SponsorOrderReview|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
