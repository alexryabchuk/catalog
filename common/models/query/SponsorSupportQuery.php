<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorSupport]].
 *
 * @see \common\models\entity\SponsorSupport
 */
class SponsorSupportQuery extends \yii\db\ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\entity\SponsorSupport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\entity\SponsorSupport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
