<?php

namespace common\models\query;

use common\models\entity\SponsorPromo;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorPromo]].
 *
 * @see \common\models\entity\SponsorPromo
 */
class SponsorPromoQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPromo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SponsorPromo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
