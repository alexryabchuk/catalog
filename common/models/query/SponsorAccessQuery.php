<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\entity\SponsorAccess]].
 *
 * @see \common\models\entity\SponsorAccess
 */
class SponsorAccessQuery extends \yii\db\ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\entity\SponsorAccess[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\entity\SponsorAccess|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
