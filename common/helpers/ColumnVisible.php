<?php


namespace common\helpers;


use Yii;

class ColumnVisible
{
    public static function getColumnVisible($tableName, $columns)
    {
        $session = Yii::$app->session;

        foreach (array_keys($columns) as $nameColumn) {
            if ($session->has($tableName . '.' . $nameColumn)) {
                $columns[$nameColumn] = $session->get($tableName . '.' . $nameColumn);
            } else {
                $columns[$nameColumn] = 'on';
                $session->set($tableName . '.' . $nameColumn, 'on');
            }
        }
        return $columns;
    }

    public static function setColumnVisible($tableName, $columns, $columnVisible)
    {
        $session = Yii::$app->session;
        foreach (array_keys($columns) as $nameColumn) {
            $columns[$nameColumn] = 'off';
        }
        foreach (array_keys($columnVisible) as $nameColumn) {
            $columns[$nameColumn] = 'on';
        }
        foreach (array_keys($columns) as $nameColumn) {
            $session->set($tableName . '.' . $nameColumn, $columns[$nameColumn]);
        }


    }
}