<?php
/* @var $filter array
 * @var $model Model
 * @var $tableName string
 */

use yii\base\Model;
use yii\helpers\Json;

?>
    <div class="modal fade bd-example-modal-sm" id="column-setting-modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Настройка отображения колонок
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    <form id="form-column-select" data-tablename="<?= $tableName ?>">
                        <?php foreach ($filter as $name => $value) : ?>
                            <p><label>
                                    <input type="checkbox"
                                           name="<?= $name ?>" <?= $value == 'on' ? 'checked' : '' ?>>
                                    <?= $model->getAttributeLabel($name) ?>
                                </label>
                            </p>
                        <?php endforeach ?>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary" id="saveColumnSetting">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
<?php
$filterJSON = Json::encode($filter);
$script = <<<JS
    $('#saveColumnSetting').on('click',function (){
    $.ajax({
        url: '/service/column-visible/index',
        type: 'POST',
        dataType: 'json',
        data: {
            'filter':$filterJSON,
            'tablename':$('#form-column-select').data('tablename'),
            'columnVisible':$('#form-column-select').serialize()
        },
        success: function (response) {
            $('#column-setting-modal').modal('hide');
            location.reload()
        },
        error: function (response) {
            krajeeDialog.alert(Yii.t('bo-deals', 'При попытке установить соединение произошла ошибка.'));
        }
    });
});
JS;
$this->registerJs($script);
?>