<?php

namespace common\widgets;

use yii\bootstrap\Widget;

class ColumnSetting extends Widget
{

    public $filter = [];
    public $tableName;
    public $model;

    public function run()
    {
        return $this->render('column-setting', [
            'filter' => $this->filter,
            'model' => $this->model,
            'tableName' => $this->tableName
        ]);
    }
}
