<?php

namespace common\widgets;

use yii\bootstrap\Tabs;
use yii\bootstrap\Widget;
use yii\helpers\Url;

class SponsorPaymentsNavBar extends Widget
{
    public $active;
    public $id;

    public function run()
    {
        return Tabs::widget([
            'navType' => 'nav-pills',
            'items' => [
                [
                    'label' => 'Методы платежей',
                    'url' => Url::to(['/sponsors/payments-method', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'payments-method',
                ],
                [
                    'label' => 'История платежей',
                    'url' => Url::to(['/sponsors/payments-history', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'payments-history',
                ],
                [
                    'label' => 'Схемы работы с партнерами',
                    'url' => Url::to(['/sponsors/payments-programs', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'payments-programs',
                ],
                [
                    'label' => 'Продажи',
                    'url' => Url::to(['/sponsors/payments-sales', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'payments-sales',
                ],
            ],
        ]);
    }
}
