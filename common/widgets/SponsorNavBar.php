<?php

namespace common\widgets;

use yii\bootstrap\Tabs;
use yii\bootstrap\Widget;
use yii\helpers\Url;

class SponsorNavBar extends Widget
{
    public $active;
    public $id;

    public function run()
    {
        return Tabs::widget([
            'items' => [
                [
                    'label' => 'Общие',
                    'url' => Url::to(['/sponsors/update', 'id' => $this->id]),
                    'active' => $this->active == 'update',
                ],
                [
                    'label' => 'Платежи',
                    'url' => Url::to(['/sponsors/payments-method', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'payments',
                ],
                [
                    'label' => 'Advertising Tools',
                    'url' => Url::to(['/sponsors/advertising', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'advertising',
                ],
                [
                    'label' => 'Связь',
                    'url' => Url::to(['/sponsors/support', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'support',
                ],
                [
                    'label' => 'Сайты',
                    'url' => Url::to(['/sponsors/sites', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'sites',
                ],
                [
                    'label' => 'Посты',
                    'url' => Url::to(['/sponsors/post', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'post',
                ],
                [
                    'label' => 'Доступ',
                    'url' => Url::to(['/sponsors/access', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'access',
                ],
                [
                    'label' => 'Backlinks',
                    'url' => Url::to(['/sponsors/backlinks', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'backlinks',
                ],
                [
                    'label' => 'Обзор и рейтинг',
                    'url' => Url::to(['/sponsors/review', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'review',
                ],
                [
                    'label' => 'Заказы обзоров',
                    'url' => Url::to(['/sponsors/order-review', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'order-review',
                ],
                [
                    'label' => 'Акции',
                    'url' => Url::to(['/sponsors/promo', 'sponsor_id' => $this->id]),
                    'active' => $this->active == 'promo',
                ],
            ],
        ]);
    }
}
