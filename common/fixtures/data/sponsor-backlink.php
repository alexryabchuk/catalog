<?php
return [
    [
        'id' => 1,
        'sponsor_id' => 11,
        'resource_page' => 'Url на сайте1',
        'period_scan' => 30,
        'backlink_status' => 1,
        'url' => 'Найденный Url1',
        'link_status' => 1,
        'first_detection' => '2020-01-01',
        'last_detection' => '2020-03-01',
    ],
    [
        'id' => 2,
        'sponsor_id' => 11,
        'resource_page' => 'Url на сайте2',
        'period_scan' => 30,
        'backlink_status' => 2,
        'url' => 'Найденный Url2',
        'link_status' => 0,
        'first_detection' => '2020-01-02',
        'last_detection' => '2020-03-02',
    ],
    [
        'id' => 3,
        'sponsor_id' => 11,
        'resource_page' => 'Url на сайте3',
        'period_scan' => 30,
        'backlink_status' => 0,
        'url' => 'Найденный Url3',
        'link_status' => 1,
        'first_detection' => '2020-01-03',
        'last_detection' => '2020-03-03',
    ],
    [
        'id' => 4,
        'sponsor_id' => 11,
        'resource_page' => 'Url на сайте4',
        'period_scan' => 30,
        'backlink_status' => 1,
        'url' => 'Найденный Url4',
        'link_status' => 1,
        'first_detection' => '2020-01-04',
        'last_detection' => '2020-03-04',
    ],
    [
        'id' => 5,
        'sponsor_id' => 11,
        'resource_page' => 'Url на сайте5',
        'period_scan' => 30,
        'backlink_status' => 2,
        'url' => 'Найденный Url5',
        'link_status' => 0,
        'first_detection' => '2020-01-05',
        'last_detection' => '2020-03-05',
    ],
    [
        'id' => 6,
        'sponsor_id' => 11,
        'resource_page' => 'Url на сайте6',
        'period_scan' => 30,
        'backlink_status' => 1,
        'url' => 'Найденный Url6',
        'link_status' => 1,
        'first_detection' => '2020-01-06',
        'last_detection' => '2020-03-06',
    ],

];
