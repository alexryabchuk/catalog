<?php
return [
    [
        'id' => 1,
        'sponsor_id' => 11,
        'param' => '1',
        'value' => '1',
        'score' => 1,
    ],
    [
        'id' => 2,
        'sponsor_id' => 11,
        'param' => '2',
        'value' => '2',
        'score' => 2,
    ],
    [
        'id' => 3,
        'sponsor_id' => 11,
        'param' => '3',
        'value' => '3',
        'score' => 3,
    ],
    [
        'id' => 4,
        'sponsor_id' => 11,
        'param' => '4',
        'value' => '4',
        'score' => 4,
    ],
    [
        'id' => 5,
        'sponsor_id' => 11,
        'param' => '5',
        'value' => '5',
        'score' => 5,
    ],
    [
        'id' => 6,
        'sponsor_id' => 11,
        'param' => '6',
        'value' => '6',
        'score' => 6,
    ],
    [
        'id' => 7,
        'sponsor_id' => 11,
        'param' => '7',
        'value' => '7',
        'score' => 7,
    ],
    [
        'id' => 8,
        'sponsor_id' => 11,
        'param' => '8',
        'value' => '8',
        'score' => 8,
    ],
    [
        'id' => 9,
        'sponsor_id' => 11,
        'param' => '9',
        'value' => '9',
        'score' => 9,
    ],

];
