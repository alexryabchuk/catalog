<?php
return [
    [
        'id' => 1,
        'sponsor_id'=> 11,
        'support_id' => 1,
        'contact'=>'Контакт1',
        'response_time'=>'1 days',
        'language'=>'Русский',
        'comment'=>'Комментарий1',
    ],
    [
        'id' => 2,
        'sponsor_id'=> 11,
        'support_id' => 2,
        'contact'=>'Контакт2',
        'response_time'=>'2 days',
        'language'=>'Русский',
        'comment'=>'Комментарий2',
    ],
    [
        'id' => 3,
        'sponsor_id'=> 11,
        'support_id' => 3,
        'contact'=>'Контакт3',
        'response_time'=>'3 days',
        'language'=>'Русский',
        'comment'=>'Комментарий3',
    ],
    [
        'id' => 4,
        'sponsor_id'=> 11,
        'support_id' => 4,
        'contact'=>'Контакт4',
        'response_time'=>'4 days',
        'language'=>'Русский',
        'comment'=>'Комментарий4',
    ],
];
