<?php
return [
    [
        'id' => 1,
        'sponsor_id' => 11,
        'name' => 'Site1',
        'pps_status' => 1,
        'category' => 'cat1',
    ],
    [
        'id' => 2,
        'sponsor_id' => 11,
        'name' => 'Site2',
        'pps_status' => 0,
        'category' => 'cat2',
    ],
    [
        'id' => 3,
        'sponsor_id' => 11,
        'name' => 'Site3',
        'pps_status' => 1,
        'category' => 'cat3',
    ],
    [
        'id' => 4,
        'sponsor_id' => 11,
        'name' => 'Site4',
        'pps_status' => 1,
        'category' => 'cat4',
    ],
    [
        'id' => 5,
        'sponsor_id' => 11,
        'name' => 'Site5',
        'pps_status' => 1,
        'category' => 'cat5',
    ],
    [
        'id' => 6,
        'sponsor_id' => 11,
        'name' => 'Site6',
        'pps_status' => 0,
        'category' => 'cat6',
    ],
    [
        'id' => 7,
        'sponsor_id' => 11,
        'name' => 'Site7',
        'pps_status' => 1,
        'category' => 'cat7',
    ],
    [
        'id' => 8,
        'sponsor_id' => 11,
        'name' => 'Site8',
        'pps_status' => 1,
        'category' => 'cat8',
    ],

];
