<?php
return [
    [
        'id' => '1',
        'name' => 'Company1',
        'slug' => 'company1',
    ],
    [
        'id' => '2',
        'name' => 'Company2',
        'slug' => 'company2',
    ],
    [
        'id' => '3',
        'name' => 'Company3',
        'slug' => 'company3',
    ],
    [
        'id' => '4',
        'name' => 'Company4',
        'slug' => 'company4',
    ],
    [
        'id' => '5',
        'name' => 'Company5',
        'slug' => 'company5',
    ],
    [
        'id' => '6',
        'name' => 'Company6',
        'slug' => 'company6',
    ],
    [
        'id' => '7',
        'name' => 'Company7',
        'slug' => 'company7',
    ],
    [
        'id' => '8',
        'name' => 'Company8',
        'slug' => 'company8',
    ],
    [
        'id' => '9',
        'name' => 'Company9',
        'slug' => 'company9',
    ],
    [
        'id' => '10',
        'name' => 'Company10',
        'slug' => 'company10',
    ],
    [
        'id' => '11',
        'name' => 'Company11',
        'slug' => 'company11',
    ],

];
