<?php
return [
    [
        'id' => 1,
        'sponsor_id' => 11,
        'name' => 'Название1',
        'image' => '',
        'content' => 'Текст1',
        'start_date' => '2020-03-01',
        'end_date' => '20-03-30',
        'url' => 'Ссылка1',
    ],
    [
        'id' => 2,
        'sponsor_id' => 11,
        'name' => 'Название2',
        'image' => '',
        'content' => 'Текст2',
        'start_date' => '2020-03-02',
        'end_date' => '20-03-30',
        'url' => 'Ссылка2',
    ],
    [
        'id' => 3,
        'sponsor_id' => 11,
        'name' => 'Название3',
        'image' => '',
        'content' => 'Текст3',
        'start_date' => '2020-03-01',
        'end_date' => null,
        'url' => 'Ссылка3',
    ],
    [
        'id' => 4,
        'sponsor_id' => 11,
        'name' => 'Название4',
        'image' => '',
        'content' => 'Текст4',
        'start_date' => '2020-03-04',
        'end_date' => '20-03-30',
        'url' => 'Ссылка4',
    ],
    [
        'id' => 5,
        'sponsor_id' => 11,
        'name' => 'Название5',
        'image' => '',
        'content' => 'Текст5',
        'start_date' => '2020-03-05',
        'end_date' => null,
        'url' => 'Ссылка5',
    ],
];
