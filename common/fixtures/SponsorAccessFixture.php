<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorAccessFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorAccess';
    public $dataFile = '@common/fixtures/data/sponsor-access.php';
    public $depends = [
        'common\fixtures\SponsorSiteFixture',
    ];
}