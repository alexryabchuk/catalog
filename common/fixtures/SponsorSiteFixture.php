<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorSiteFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorSites';
    public $dataFile = '@common/fixtures/data/sponsor-site.php';
}