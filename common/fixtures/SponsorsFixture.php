<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorsFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\Sponsors';
}