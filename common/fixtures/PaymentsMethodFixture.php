<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class PaymentsMethodFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPaymentsMethod';
    public $dataFile = '@common/fixtures/data/sponsor-payments-method.php';
}