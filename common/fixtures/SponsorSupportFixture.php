<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorSupportFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorSupport';
    public $dataFile = '@common/fixtures/data/sponsor-support.php';
}