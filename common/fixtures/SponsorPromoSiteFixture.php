<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorPromoSiteFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPromoSites';
    public $dataFile = '@common/fixtures/data/sponsor-promo-site.php';
//    public $depends = [
//        'common\fixtures\SponsorPromo',
//    ];
}