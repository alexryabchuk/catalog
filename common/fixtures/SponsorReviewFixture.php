<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorReviewFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorsReview';
    public $dataFile = '@common/fixtures/data/sponsor-review.php';
}