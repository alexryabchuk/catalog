<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorPromoFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPromo';
    public $dataFile = '@common/fixtures/data/sponsor-promo.php';
    public $depends = [
        'common\fixtures\SponsorSiteFixture',
    ];
}