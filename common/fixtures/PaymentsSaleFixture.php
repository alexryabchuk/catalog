<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class PaymentsSaleFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPaymentsSales';
    public $dataFile = '@common/fixtures/data/sponsor-payments-sales.php';
}