<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class GeneralCompanyFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\GeneralCompany';
    public $dataFile = '@common/fixtures/data/general-company.php';
}