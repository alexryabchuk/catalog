<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class PaymentsProgramFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPaymentsProgram';
    public $dataFile = '@common/fixtures/data/sponsor-payments-program.php';
}