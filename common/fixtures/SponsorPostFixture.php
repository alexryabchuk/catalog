<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorPostFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPost';
    public $dataFile = '@common/fixtures/data/sponsor-post.php';
    public $depends = [
        'common\fixtures\SponsorSiteFixture',
    ];
}