<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class PaymentsHistoryFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorPaymentsHistory';
    public $dataFile = '@common/fixtures/data/sponsor-payments-history.php';
}