<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorOrderReviewFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorOrderReview';
    public $dataFile = '@common/fixtures/data/sponsor-order-review.php';
}