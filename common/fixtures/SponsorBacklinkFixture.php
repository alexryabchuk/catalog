<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class SponsorBacklinkFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\SponsorBacklinks';
    public $dataFile = '@common/fixtures/data/sponsor-backlink.php';
    public $depends = [
        'common\fixtures\SponsorSiteFixture',
    ];
}