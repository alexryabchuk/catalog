<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_systems}}`.
 */
class m210311_213008_create_payment_systems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_systems}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'description' => $this->text(),
            'image' => $this->string(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->insert('{{%payment_systems}}',['name'=>'Paxum', 'slug'=>'paxum']);
        $this->insert('{{%payment_systems}}',['name'=>'Wire Transfer SWIFT', 'slug'=>'wire-transfer-swift']);
        $this->insert('{{%payment_systems}}',['name'=>'Wire Transfer Domestic', 'slug'=>'wire-transfer-domestic']);
        $this->insert('{{%payment_systems}}',['name'=>'Wire Transfer SEPA', 'slug'=>'wire-transfer-domestic']);
        $this->insert('{{%payment_systems}}',['name'=>'Check', 'slug'=>'check']);
        $this->insert('{{%payment_systems}}',['name'=>'Web Money', 'slug'=>'web-money']);
        $this->insert('{{%payment_systems}}',['name'=>'PayPal', 'slug'=>'paypal']);
        $this->insert('{{%payment_systems}}',['name'=>'Payoneer', 'slug'=>'payoneer']);
        $this->insert('{{%payment_systems}}',['name'=>'Bitcoins', 'slug'=>'bitcoins']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_systems}}');
    }
}
