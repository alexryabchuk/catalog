<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%CMSSponsor}}`.
 */
class m210313_095251_create_CMSSponsor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%CMSSponsor}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'description' => $this->text(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->insert('{{%CMSSponsor}}',['name'=>'NATs', 'slug'=>'nats']);
        $this->insert('{{%CMSSponsor}}',['name'=>'Own Dev', 'slug'=>'own-dev']);
        $this->insert('{{%CMSSponsor}}',['name'=>'MPA', 'slug'=>'mpa']);
        $this->insert('{{%CMSSponsor}}',['name'=>'CCBILL', 'slug'=>'ccbill']);
        $this->insert('{{%CMSSponsor}}',['name'=>'Epoch', 'slug'=>'epoch']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%CMSSponsor}}');
    }
}
