<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_backlinks}}`.
 */
class m210328_153442_create_sponsor_backlinks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_backlinks}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull()->comment('Спонсор'),
            'resource_page' => $this->string()->notNull()->comment('Url на сайте'),
            'period_scan' => $this->integer()->notNull()->defaultValue(30)->comment('Частота парсинга'),
            'backlink_status' => $this->integer()->notNull()->defaultValue(0)->comment(''),
            'url' => $this->string()->notNull()->comment('Найденный Url'),
            'link_status' => $this->boolean()->comment('Статус ссылки'),
            'first_detection' => $this->date()->comment(''),
            'last_detection' => $this->date()->comment(''),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('fk-sponsor_backlinkss-sponsor_id-sponsors-id', '{{%sponsor_backlinks}}',
            'sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%backlinks}}');
    }
}
