<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_promo}}`.
 */
class m210329_093644_create_sponsor_promo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_promo}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull()->comment('Спонсор'),
            'name' => $this->string()->notNull()->comment('Название'),
            'image' => $this->string()->comment('Картинка'),
            'content' => $this->text()->comment('Текст'),
            'start_date' => $this->date()->notNull()->comment('Дата начала'),
            'end_date' => $this->date()->comment('Дата конца'),
            'url' => $this->string()->notNull()->comment('Ссылка'),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('fk-sponsor_promo-sponsor_id-sponsors-id', '{{%sponsor_promo}}',
            'sponsor_id', '{{%sponsors}}', 'id');
        $this->createTable('{{%sponsor_promo_sites}}', [
            'id' => $this->primaryKey(),
            'promo_id' => $this->integer()->notNull(),
            'sites_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk-sponsor_promo_sites-promo_id-promo-id', '{{%sponsor_promo_sites}}',
            'promo_id', '{{%sponsor_promo}}', 'id');
        $this->addForeignKey('fk-sponsor_promo_sites-sites_id-sponsor_sites-id', '{{%sponsor_promo_sites}}',
            'sites_id', '{{%sponsor_sites}}', 'id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_promo_sites}}');
        $this->dropTable('{{%sponsor_promo}}');
    }
}
