<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%support}}`.
 */
class m210313_075236_create_support_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%support}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'slug' => $this->string(),
            'is_deleted' => $this->boolean()->defaultValue(false)
        ]);
        $this->insert('{{%support}}',['name'=>'e-mail','slug'=>'e-mail']);
        $this->insert('{{%support}}',['name'=>'tickers','slug'=>'tickers']);
        $this->insert('{{%support}}',['name'=>'skype','slug'=>'skype']);
        $this->insert('{{%support}}',['name'=>'icq','slug'=>'icq']);
        $this->insert('{{%support}}',['name'=>'telegram','slug'=>'telegram']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%support}}');
    }
}
