<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_order_review}}`.
 */
class m210329_064030_create_sponsor_order_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_order_review}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull()->comment('Спонсор'),
            'order_date' => $this->date()->notNull()->comment('Дата'),
            'status' => $this->integer()->notNull()->defaultValue(1)->comment('Статус'),
            'e_mail' => $this->string()->notNull()->comment('E-mail'),
            'order_sponsor_id' => $this->integer()->comment('Спонсор'),
            'order_url' => $this->string()->comment('URL'),
            'recip_URL' => $this->string()->notNull()->comment('Recip URL'),
            'editor' => $this->string()->notNull()->comment('Редактор'),
            'writer' => $this->string()->notNull()->comment('Писатель'),
            'pay_amount' => $this->decimal(10, 2)->comment('Сумма'),
            'pay_status' => $this->boolean()->defaultValue(false),
            'is_deleted' => $this->boolean()->defaultValue(false)
        ]);
        $this->addForeignKey('fk-sponsor_order_review-sponsor_id-sponsors-id', '{{%sponsor_order_review}}',
            'sponsor_id', '{{%sponsors}}', 'id');
        $this->addForeignKey('fk-sponsor_order_review-order_sponsor_id-sponsors-id', '{{%sponsor_order_review}}',
            'order_sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_order_review}}');
    }
}
