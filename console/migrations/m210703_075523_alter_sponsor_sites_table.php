<?php

use yii\db\Migration;

/**
 * Class m210703_075523_alter_sponsor_sites_table
 */
class m210703_075523_alter_sponsor_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%sponsor_sites}}', 'category');
        $this->addColumn('{{%sponsor_sites}}', 'main_category', $this->integer());
        $this->addColumn('{{%sponsor_sites}}', 'category1', $this->integer());
        $this->addColumn('{{%sponsor_sites}}', 'category2', $this->integer());
        $this->addColumn('{{%sponsor_sites}}', 'category3', $this->integer());
        $this->addColumn('{{%sponsor_sites}}', 'category4', $this->integer());
        $this->addColumn('{{%sponsor_sites}}', 'category5', $this->integer());
        $this->addForeignKey('fk-sponsor_sites-main_category-category_sites-id', '{{%sponsor_sites}}',
            'main_category', '{{%category_sites}}', 'id');
        $this->addForeignKey('fk-sponsor_sites-category1-category_sites-id', '{{%sponsor_sites}}',
            'category1', '{{%category_sites}}', 'id');
        $this->addForeignKey('fk-sponsor_sites-category2-category_sites-id', '{{%sponsor_sites}}',
            'category2', '{{%category_sites}}', 'id');
        $this->addForeignKey('fk-sponsor_sites-category3-category_sites-id', '{{%sponsor_sites}}',
            'category3', '{{%category_sites}}', 'id');
        $this->addForeignKey('fk-sponsor_sites-category4-category_sites-id', '{{%sponsor_sites}}',
            'category4', '{{%category_sites}}', 'id');
        $this->addForeignKey('fk-sponsor_sites-category5-category_sites-id', '{{%sponsor_sites}}',
            'category5', '{{%category_sites}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%sponsor_sites}}', 'category', $this->string());
        $this->dropForeignKey('fk-sponsor_sites-main_category-category_sites-id','{{%sponsor_sites}}');
        $this->dropColumn('{{%sponsor_sites}}', 'main_category');
        $this->dropForeignKey('fk-sponsor_sites-category1-category_sites-id','{{%sponsor_sites}}');
        $this->dropForeignKey('fk-sponsor_sites-category2-category_sites-id','{{%sponsor_sites}}');
        $this->dropForeignKey('fk-sponsor_sites-category3-category_sites-id','{{%sponsor_sites}}');
        $this->dropForeignKey('fk-sponsor_sites-category4-category_sites-id','{{%sponsor_sites}}');
        $this->dropForeignKey('fk-sponsor_sites-category5-category_sites-id','{{%sponsor_sites}}');
        $this->dropColumn('{{%sponsor_sites}}', 'category1');
        $this->dropColumn('{{%sponsor_sites}}', 'category2');
        $this->dropColumn('{{%sponsor_sites}}', 'category3');
        $this->dropColumn('{{%sponsor_sites}}', 'category4');
        $this->dropColumn('{{%sponsor_sites}}', 'category5');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210703_075523_alter_sponsor_sites_table cannot be reverted.\n";

        return false;
    }
    */
}
