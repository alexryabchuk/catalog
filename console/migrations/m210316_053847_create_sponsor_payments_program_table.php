<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_payments_program}}`.
 */
class m210316_053847_create_sponsor_payments_program_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_payments_program}}', [
            'id' => $this->primaryKey(),
            'sponsor_id'=>$this->integer()->notNull()->comment('Спонсор'),
            'type' => $this->integer()->notNull()->comment('Тип программы'),
            'sum_percent' => $this->decimal(14,2)->notNull()->comment('СУмма/Процент'),
            'comment'=>$this->text()->comment('Коментарий'),
            'is_deleted'=>$this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('fk-payments_program-sponsor_id-sponsors-id', '{{%sponsor_payments_program}}',
            'sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_payments_program}}');
    }
}
