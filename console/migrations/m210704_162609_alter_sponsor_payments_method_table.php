<?php

use yii\db\Migration;

/**
 * Class m210704_162609_alter_sponsor_payments_method_table
 */
class m210704_162609_alter_sponsor_payments_method_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%sponsor_payments_method}}','fee' , $this->decimal(14, 2)->null());
        $this->alterColumn('{{%sponsor_payments_method}}','hold' , $this->decimal(14, 2)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%sponsor_payments_method}}','fee' , $this->decimal(14, 2)->notNull());
        $this->alterColumn('{{%sponsor_payments_method}}','hold' , $this->decimal(14, 2)->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210704_162609_alter_sponsor_payments_method_table cannot be reverted.\n";

        return false;
    }
    */
}
