<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_post}}`.
 */
class m210328_092626_create_sponsor_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_post}}', [
            'id' => $this->primaryKey(),
            'sponsor_id'=>$this->integer()->notNull()->comment('Спонсор'),
            'post_date'=>$this->date()->notNull()->comment('Дата'),
            'status'=>$this->boolean()->defaultValue(false)->comment('Статус'),
            'assign_sponsor_id'=>$this->integer()->comment('Спонсор'),
            'assign_site_id'=>$this->integer()->comment('Сайт'),
            'post'=>$this->text()->comment('Текст'),
            'is_deleted'=>$this->boolean()->defaultValue(false)->comment('Статус'),
        ]);
        $this->addForeignKey('fk-sponsor_post-sponsor_id-sponsors-id', '{{%sponsor_post}}',
            'sponsor_id', '{{%sponsors}}', 'id');
        $this->addForeignKey('fk-sponsor_post-assign_sponsor_id-sponsors-id', '{{%sponsor_post}}',
            'assign_sponsor_id', '{{%sponsors}}', 'id');
        $this->addForeignKey('fk-sponsor_post-assign_site_id-sponsors-id', '{{%sponsor_post}}',
            'assign_site_id', '{{%sponsor_sites}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_post}}');
    }
}
