<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_payments_sales}}`.
 */
class m210316_100714_create_sponsor_payments_sales_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_payments_sales}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull()->comment('Спонсор'),
            'period_month' => $this->integer(2)->notNull()->comment('Период(месяц)'),
            'period_years' => $this->integer(4)->notNull()->comment('Период(год)'),
            'unq' => $this->integer()->notNull()->comment('UNQ'),
            'sales' => $this->integer()->notNull()->comment('Sales'),
            'sales_usd' => $this->decimal(14, 2)->notNull()->comment('Sales USD'),
            'rebills' => $this->integer()->notNull()->comment('Rebills'),
            'rebills_usd' => $this->decimal(14, 2)->notNull()->comment('Rebills USD'),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('fk-payments_sales-sponsor_id-sponsors-id', '{{%sponsor_payments_sales}}',
            'sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_payments_sales}}');
    }
}
