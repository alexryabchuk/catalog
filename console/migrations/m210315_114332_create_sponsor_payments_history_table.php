<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_payments_history}}`.
 */
class m210315_114332_create_sponsor_payments_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_payments_history}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull(),
            'payments_method_id' => $this->integer()->notNull(),
            'date_pay' => $this->date()->notNull(),
            'sum_pay' => $this->decimal()->notNull(),
            'status' => $this->integer(),
            'is_deleted' => $this->boolean()->defaultValue(false)
        ]);
        $this->addForeignKey('fk-payments_history-sponsor_id-sponsors-id', '{{%sponsor_payments_history}}',
            'sponsor_id', '{{%sponsors}}', 'id');
        $this->addForeignKey('fk-payments_history-payments_method_id-payments_method-id',
            '{{%sponsor_payments_history}}', 'payments_method_id', '{{%sponsor_payments_method}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_payments_history}}');
    }
}
