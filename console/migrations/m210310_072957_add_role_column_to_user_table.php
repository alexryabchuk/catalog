<?php

use common\models\enums\UserRole;
use common\models\enums\UserStatus;
use common\models\User;
use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m210310_072957_add_role_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'role', $this->smallInteger()->defaultValue(UserRole::USER));
        $user = new User();
        $user->username = 'admin';
        $user->email = 'admin@admin.com';
        $user->setPassword('12345678');
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->status = UserStatus::STATUS_ACTIVE;
        $user->role = UserRole::ADMIN;
        $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $user = User::find()->where(['username' => 'admin'])->one();
        if ($user) $user->delete();
        $this->dropColumn('{{%user}}', 'role');
    }
}
