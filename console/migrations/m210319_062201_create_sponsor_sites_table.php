<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_sites}}`.
 */
class m210319_062201_create_sponsor_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_sites}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->comment('Спонсор'),
            'name' => $this->string()->comment('Имя сайта'),
            'pps_status' => $this->integer()->comment('PPS статус'),
            'category'=>$this->string()->comment('Категория'),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('fk-sponsor_sites-sponsor_id-sponsors-id', '{{%sponsor_sites}}',
            'sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_sites}}');
    }
}
