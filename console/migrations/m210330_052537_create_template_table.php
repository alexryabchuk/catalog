<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%template}}`.
 */
class m210330_052537_create_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%template}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'template_php' => $this->text()->comment('Шаблон PHP'),
            'template_smarty' => $this->text()->comment('Шаблон Smarty'),
            'template_blade' => $this->text()->comment('Шаблон Blade'),
            'default_template' => $this->integer()->notNull()->defaultValue(1),
        ]);
        $this->insert('{{%template}}', ['id' => 1, 'name' => 'Главная страница']);
        $this->insert('{{%template}}', ['id' => 2, 'name' => 'Список спонсоров']);
        $this->insert('{{%template}}', ['id' => 3, 'name' => 'Спонсор']);
        $this->insert('{{%template}}', ['id' => 4, 'name' => 'Публикация постов']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%template}}');
    }
}
