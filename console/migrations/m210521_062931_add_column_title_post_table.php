<?php

use yii\db\Migration;

/**
 * Class m210521_062931_add_column_title_post_table
 */
class m210521_062931_add_column_title_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sponsor_post}}', 'title',$this->string()->notNull()->comment('Заголовок'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sponsor_post}}', 'title');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210521_062931_add_column_title_post_table cannot be reverted.\n";

        return false;
    }
    */
}
