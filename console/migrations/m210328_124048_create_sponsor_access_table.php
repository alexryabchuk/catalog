<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_access}}`.
 */
class m210328_124048_create_sponsor_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_access}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull()->comment('Спонсор'),
            'sponsor_site_id' => $this->integer()->notNull()->comment('Сайт'),
            'login' => $this->string(64)->notNull()->comment(''),
            'password' => $this->string(64)->notNull()->comment(''),
            'actual_date' => $this->date()->comment(''),
            'status' => $this->integer()->notNull()->comment(''),
            'comment' => $this->string()->comment(''),
            'is_deleted' => $this->boolean()->defaultValue(false)
        ]);
        $this->addForeignKey('fk-sponsor_access-assign_sponsor_id-sponsors-id', '{{%sponsor_access}}',
            'sponsor_id', '{{%sponsors}}', 'id');
        $this->addForeignKey('fk-sponsor_access-assign_site_id-sponsors-id', '{{%sponsor_access}}',
            'sponsor_site_id', '{{%sponsor_sites}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_access}}');
    }
}
