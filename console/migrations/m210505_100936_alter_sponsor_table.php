<?php

use yii\db\Migration;

/**
 * Class m210505_100936_alter_sponsor_table
 */
class m210505_100936_alter_sponsor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%sponsors}}', 'ad_trafic', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_banners', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_fhg', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_hosted', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_downloadable', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_embedded', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_models', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%sponsors}}', 'ad_trafic', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_banners', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_fhg', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_hosted', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_downloadable', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_embedded', $this->boolean()->defaultValue(false));
        $this->alterColumn('{{%sponsors}}', 'ad_models', $this->boolean()->defaultValue(false));
    }

}
