<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_support}}`.
 */
class m210316_145417_create_sponsor_support_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_support}}', [
            'id' => $this->primaryKey(),
            'sponsor_id'=> $this->integer()->notNull()->comment('Спонсор'),
            'support_id' => $this->integer()->notNull()->comment('Тип связи'),
            'contact'=>$this->string()->comment('Контакт'),
            'response_time'=>$this->string()->comment('Время ответа'),
            'language'=>$this->string()->comment('Язык'),
            'comment'=>$this->string()->comment('Комментарий'),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('fk-sponsor_support-sponsor_id-sponsors-id', '{{%sponsor_support}}',
            'sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_support}}');
    }
}
