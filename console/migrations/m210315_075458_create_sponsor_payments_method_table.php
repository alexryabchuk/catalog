<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsor_payments_method}}`.
 */
class m210315_075458_create_sponsor_payments_method_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsor_payments_method}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull(),
            'payment_system_id' => $this->integer()->notNull(),
            'min_pay' => $this->decimal(14, 2)->notNull(),
            'fee' => $this->decimal(14, 2)->notNull(),
            'hold' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('fk-sponsor_payments_method-sponsor_id-sponsors-id', '{{%sponsor_payments_method}}',
            'sponsor_id', '{{%sponsors}}', 'id');
        $this->addForeignKey('fk-sponsor_payments_method-payment_system_id-payment_system-id',
            '{{%sponsor_payments_method}}',
            'payment_system_id', '{{payment_systems}}', 'id');
        $this->createIndex('idx-sponsor_payments_method-payment_system_id', '{{%sponsor_payments_method}}',
            ['sponsor_id', 'payment_system_id'], true);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsor_payments_method}}');
    }
}
