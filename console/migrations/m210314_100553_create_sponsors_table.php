<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsors}}`.
 */
class m210314_100553_create_sponsors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sponsors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'ref_url' => $this->string()->notNull(),
            'ref_url_date_scan' => $this->string(),
            'ref_url_status_scan' => $this->string(),
            'domain' => $this->string(),
            'description' => $this->text(),
            'publication_status' => $this->boolean()->defaultValue(true),
            'recommendation_status' => $this->boolean()->defaultValue(true),
            'not_recommendation_reason' => $this->text(),
            'general_company_id' => $this->integer(),
            'cms_id' => $this->integer(),
            'image' => $this->string(),
            'is_deleted' => $this->boolean()->defaultValue(false)
        ]);
        $this->addForeignKey('fk-sponsors-general_company_id-general_company-id', '{{%sponsors}}',
            'general_company_id', '{{%general_company}}', 'id');
        $this->addForeignKey('fk-sponsors-cms_id-CMSSponsor-id', '{{%sponsors}}',
            'cms_id', '{{%CMSSponsor}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sponsors}}');
    }
}
