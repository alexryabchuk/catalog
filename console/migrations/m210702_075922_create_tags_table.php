<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tags}}`.
 */
class m210702_075922_create_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tags}}', [
            'id' => $this->primaryKey(),
			'parent_id' => $this->integer()->comment(''),
			'name' => $this->string()->comment('Слаг'),
			'slug' => $this->string()->comment('Слаг'),
			'description' => $this->text()->comment('Описание'),
			'image' => $this->string(512)->comment('Изображение'),
			'is_visible' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tags}}');
    }
}
