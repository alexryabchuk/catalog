<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category_sites}}`.
 */
class m210702_075852_create_category_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category_sites}}', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->comment('Название'),
			'slug' => $this->string()->comment('Slug(Папка)'),
			'description' => $this->text()->comment('Описание'),
			'image' => $this->string(512)->comment('Изображение'),
			'is_active' => $this->boolean()->comment('Статус'),
			'is_deleted' => $this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_sites}}');
    }
}
