<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sponsors_review}}`.
 */
class m210328_172512_create_sponsors_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sponsors}}', 'review_text', $this->text());
        $this->createTable('{{%sponsors_review}}', [
            'id' => $this->primaryKey(),
            'sponsor_id' => $this->integer()->notNull()->comment('Спонсор'),
            'param' => $this->string()->notNull()->comment(''),
            'value' => $this->string()->notNull()->comment(''),
            'score' => $this->decimal(5, 1)->notNull()->comment('')
        ]);
        $this->addForeignKey('fk-sponsors_review-sponsor_id-sponsors-id', '{{%sponsors_review}}',
            'sponsor_id', '{{%sponsors}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%review}}');
    }
}
