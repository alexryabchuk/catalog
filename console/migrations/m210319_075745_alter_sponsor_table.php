<?php

use yii\db\Migration;

/**
 * Class m210319_075745_alter_sponsor_table
 */
class m210319_075745_alter_sponsor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sponsors}}','ad_trafic',$this->boolean()->defaultValue(true));
        $this->addColumn('{{%sponsors}}','ad_banners',$this->boolean()->defaultValue(true));
        $this->addColumn('{{%sponsors}}','ad_fhg',$this->boolean()->defaultValue(true));
        $this->addColumn('{{%sponsors}}','ad_hosted',$this->boolean()->defaultValue(true));
        $this->addColumn('{{%sponsors}}','ad_downloadable',$this->boolean()->defaultValue(true));
        $this->addColumn('{{%sponsors}}','ad_embedded',$this->boolean()->defaultValue(true));
        $this->addColumn('{{%sponsors}}','ad_models',$this->boolean()->defaultValue(true));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sponsors}}','ad_trafic');
        $this->dropColumn('{{%sponsors}}','ad_banners');
        $this->dropColumn('{{%sponsors}}','ad_fhg');
        $this->dropColumn('{{%sponsors}}','ad_hosted');
        $this->dropColumn('{{%sponsors}}','ad_downloadable');
        $this->dropColumn('{{%sponsors}}','ad_embedded');
        $this->dropColumn('{{%sponsors}}','ad_models');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210319_075745_alter_sponsor_table cannot be reverted.\n";

        return false;
    }
    */
}
