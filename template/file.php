<?php

/* @var $this yii\web\View */

// Подключения сервисного. Нужно для использования получения данних
use backend\models\service\SponsorsService;
use common\models\entity\Sponsors;
use common\models\enums\PaymentsProgramTypes;

// Создание екземляра класса

$sponsorsService = new SponsorsService();
// Получение спонсора
/* @var $sponsor Sponsors */

$sponsor = $sponsorsService->getSponsorInfo($id);

$this->title = 'Каталог партнерских программ';
?>

<div class="site-index">
    <div class="row">
        <div class="col-md-12" style="text-align: center"><h1>sponsor</h1></div>
        <div class="col-md-12" style="background-color: #1e7e34;padding: 25px; text-align: center">header</div>
        <div class="col-md-12">
            <h2><?= $sponsor->name ?></h2>
            <span class="pull-right"><i class="fa fa-flag" style="font-size: 20px"> 99%</i> </span>
        </div>
        <div class="col-md-12">
            <div style="float: left; width: 400px">
                <img src="<?= $sponsor->image ?>" style="width: 400px">
            </div>
            <div style="margin-left: 420px">
                <ul>
                    <?php foreach ($sponsor->sponsorsReviews as $sponsorsReview) : ?>
                        <li><?= $sponsorsReview->value ?> <?= $sponsorsReview->score ?>%</li>
                    <?php endforeach; ?>
                </ul>
                Recommendation
                status <?= $sponsor->recommendation_status ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                <br>
                <span class="btn btn-warning"><a href="<?= $sponsor->ref_url ?>">Join Sponsor to make money</a> </span>
            </div>

        </div>
        <div class="col-md-12"><?= $sponsor->description ?></div>
        <div class="col-md-12">
            <br>
            <b>Programms:</b><br>
            <ul>
                <?php foreach ($sponsor->sponsorPaymentsPrograms as $p) : ?>
                    <li><?= PaymentsProgramTypes::getLabel($p->type) ?>: <?= $p->sum_percent ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-12">
            <br>
            <b>Company: </b> <?= $sponsor->generalCompany->name ?>
        </div>
        <div class="col-md-12">
            <b>CMS: </b> <?= $sponsor->cms->name ?>
        </div>
        <div class="col-md-12">
            <br>
            <b>Payment methods and minimum amount:</b><br>
            <ul>
                <?php foreach ($sponsor->sponsorPaymentsMethods as $p) : ?>
                    <li><?= $p->paymentSystem->name ?>: (<?= $p->min_pay ?>$)</li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-12">
            <br>
            <b>Advertising Tools</b>
            <div> <?= $sponsor->ad_trafic ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                Traffic Campaign
            </div>
            <div> <?= $sponsor->ad_banners ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                Banners
            </div>
            <div> <?= $sponsor->ad_fhg ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>xml
            </div>
            <div> <?= $sponsor->ad_hosted ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                Hosted Content
            </div>
            <div> <?= $sponsor->ad_downloadable ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                Downloadable Content
            </div>
            <div> <?= $sponsor->ad_embedded ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                Embedded Content
            </div>
            <div> <?= $sponsor->ad_models ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-close"></i>' ?>
                Models Link
            </div>
        </div>
        <div class="col-md-12">
            <br>
            <b>Support</b>
            <?php foreach ($sponsor->sponsorSupports as $p) : ?>
                <li><?= $p->support->name ?>: <?= $p->contact ?></li>
            <?php endforeach; ?>
        </div>
        <div class="col-md-12">
            <br>
            <b>Sites (<?= count($sponsor->sponsorSites) ?>)</b>
            <?php foreach ($sponsor->sponsorSites as $p) : ?>
                <a href="<?= $p->name ?>" style="display:block"> <i class="fa  fa-external-link"></i> <?= $p->name ?>
                </a>
            <?php endforeach; ?>
        </div>
        <div class="col-md-12">
            <br>
            <b>News </b>
            <?php foreach ($sponsor->sponsorPosts as $p) : ?>
                <div><?= date('d.m.Y', strtotime($p->post_date)) . ' ' . $p->title ?></div>
            <?php endforeach; ?>
        </div>
        <div class="col-md-12">
            <br>
            <b>Review </b>
            <br>
            <?=$sponsor->review_text?>
            <br>
        </div>
        <div class="col-md-12">
            <span style="display:block;width: 100%" class="btn btn-warning"><a href="<?= $sponsor->ref_url ?>">Join Sponsor to make money</a> </span>
            <br>
        </div>
        <div class="col-md-12" style="background-color: #0a73bb;padding: 25px; text-align: center">footer</div>
    </div>
</div>
